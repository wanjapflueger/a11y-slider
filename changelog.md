# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed

- Bumped Node version from `12` to `20`
- Improved documentation on several functions by adding JSDoc `@example` and filling out missing data
- Project files will be formatted with prettier (before only the files in [./src](./src) were formatted)
- Name of project owner

### Added

- Information on how to import SCSS with JavaScript in [README.md](README.md)
- Improved documentation on how to work with arrows and their translated labels

## [v2.3.4] - 2022-03-06

### Removed

- GET _config_ (since its useless)

### Changed

- GET _elements_: renamed `elements.paginationBullets` to `elements.bullets`
- Updated README

### Added

- CSS selectors on previous and next buttons: `[data-a11y-slider-arrow]` on both and `[data-a11y-slider-previous]` on previous and `[data-a11y-slider-next]` on next. It is now easier to target slider arrows with CSS.
- CSS selectors on pagination bullets: `[data-a11y-slider-bullet]`. It is now easier to target pagination bullets with CSS.

## [v2.3.3] - 2022-03-06

### Fixed

- Synced sliders would be moved twice, once on arrow click and once due to scroll event that was fired after the arrow click event. This caused jittery behaviour.

## [v2.3.2] - 2022-03-06

### Fixed

- `syncWith` would not work when no `onMoveEnd` callback function was attached

## [v2.3.1] - 2022-03-06

### Added

- Prevent browser history change on swipe with `overscroll-behavior-x` (addresses [this issue](https://gitlab.com/wanjapflueger/a11y-slider/-/issues/4))

## [v2.3.0] - 2022-02-28

### Changed

- README

## [v2.2.0] - 2022-02-28

### Added

- Parameter `syncWith` can be used to sync multiple instances of `Slider` (addresses [this issue](https://gitlab.com/wanjapflueger/a11y-slider/-/issues/7))

## [v2.1.0] - 2022-02-25

### Added

- `status.currentIndex` is a number that represents the current slider index (addresses [this issue](https://gitlab.com/wanjapflueger/a11y-slider/-/issues/6))

## [v2.0.4] - 2022-02-08

### Fixed

- SCSS error

## [v2.0.3] - 2022-02-08

### Changed

- Adjust the calculation of `slide-sizing` related to [this issue](https://gitlab.com/wanjapflueger/a11y-slider/-/issues/5)

## [v2.0.2] - 2022-02-03

### Changed

- When having a large space-before we want to avoid that the user has to click twice to go to the second slide. So we send the user directly to slide 2.

## [v2.0.1] - 2022-02-03

### Fixed

- When choosing a value for the space between slides, that was greater than the actual size of the slides, the slider would not move at all. Now the calculation of the distance to move the slider also considers the computed space between slides.

## [v2.0.0] - 2022-01-18

## Changed

- Upgraded node modules
- Breaking Change: Slash as Division. Now using `math.div` instead of `/` in _SASS_. Read more in [this article](https://sass-lang.com/documentation/breaking-changes/slash-div). Addresses [Sass Slash as Division](https://gitlab.com/wanjapflueger/a11y-slider/-/issues/3). Does not work with deprecated [Node Sass](https://www.npmjs.com/package/node-sass) anymore, now requires [SASS](https://www.npmjs.com/package/sass).
- Will use a major version number for future releases containing breaking changes

## [v1.7.0] - 2022-01-14

### Changed

- It is now possible to scroll the slider, while the mouse is hovering the „previous” or „next button”. This is especially usefull, when the previous or next arrows cover a large area of the slider itself. This is acchieved by setting `pointer-events: none` on an arrow, while the user is scrolling (`WheelEvent`).

## [v1.6.4] - 2022-01-11

### Fixed

- Calculation in `SliderSlidesIsLastSlide()` is now based on the `scrollLeft` value instead of a `DOMRect` value. This fixes a rare problem where the last slide was displayed but `SliderSlidesIsLastSlide()` would return `false`. Occured only when the sliders `width` was smaller than the `window` width.

## [v1.6.3] - 2022-01-11

### Fixed

- Calculation in `SliderSlidesIsFirstSlide()` is now based on the `scrollLeft` value instead of a `DOMRect` value. This fixes a rare problem where the user could not use the "previous arrow" even though the second slide was displayed.

### Added

- `isFirst` and `isLast` on interface `Status`

## [v1.6.2] - 2022-01-02

### Fixed

- A bug when the space between slides was greater than the space before or after, the space before or after would become `0`

## [v1.6.1] - 2021-12-30

### Fixed

- [Autoplay not running after click interaction with slider](https://gitlab.com/wanjapflueger/a11y-slider/-/issues/2)

### Changed

- Since [v1.5.0] Safari 14.0 was added to the list of supported browsers. This information was now updated in [README.md#browser-support](README.md#browser-support)

## [v1.6.0] - 2021-12-01

### Added

- New parameter `onDestroyed` accepts a callback function that is called once, when the slider was successfully destroyed
- Added „Issues” to README

### Fixed

- New parameter `onCreated` accepts a callback function that is called once, when the slider was successfully created. Adresses an issue, when event listeners would not work inside the slider (see [README.md#event-listener-do-not-work](README.md#event-listener-do-not-work) and [Issue](https://gitlab.com/wanjapflueger/a11y-slider/-/issues/1))

## [v1.5.2] - 2021-11-21

### Fixed

- The calculation of the space before and after did not work under certain circumstances when using `margin` so this is replaced with `flex` and `width`.

## [v1.5.1] - 2021-11-14

### Fixed

- `[data-a11y-slider-slide-spacer]` was not displayed with the correct size

## [v1.5.0] - 2021-11-14

### Added

- Parameter `loop`
- Parameter `onUpdate` accepts a callback function and delivers status information like _progress_  from `0` to `100`.

### Fixed

- Use `margin` instead of `gap` to display space between slides fixes for Safari `< 14.1` (@see [https://caniuse.com/flexbox-gap](https://caniuse.com/flexbox-gap))

## [v1.4.3] - 2021-09-22

### Fixed

- Using `calc` function in mixin `slide-sizing` in parameter `$before` and `$after`

## [v1.4.2] - 2021-09-20

### Fixed

- Always have an active bullet in pagination (for real this time)

## [v1.4.1] - 2021-09-20

### Changed

- Pagination and arrows have `[aria-hidden="true"]` and `[aria-disabled="true"]` when only 1 slide is in slider

### Added

- [README.md#browser-support](README.md#browser-support)

### Fixed

- In Mixin `slide-sizing` fixed a bug when `$spacing` being greater than `$before` resulted in a _space before_ that was greater than the value of `$before`. Now the _space before_ is always the size that is defined in `$before`. Same thing with `$after`.
- After initializing the slider, when pressing the _next_ button, the slider would go to the first item. But it should go to the second one. Now it does that.

## [v1.4.0] - 2021-09-14

### Fixed

- Attention! current item in pagination has attribute `[aria-current="true"]`. Was on the `HTMLButtonElement` is now on the `HTMLLiElement` (`parentElement`)
- Always have an active bullet in pagination
- Attributes on bullets are only updated, if another bullet is the new current bullet (was on scroll before)
## [v1.3.1] - 2021-08-18

### Fixed

- Scroll margin was not unset when using different breakpoints

### Updated

- Readme

## [v1.3.0] - 2021-07-30

### Breaking

- Parameter `drag` was removed for various reasons: 1.) slides with images tend to mess up dragging with image-drag. 2.) Selecting text on a draggable slider is not possible. 3.) The dragging behaviour was not smooth at all and messing up with scroll-snap

### Changed

- The visibility of elements like slides and buttons is now also updated on window scroll with animation keyframing
- The visibility of elements like slides and buttons is now also updated on window resize with animation keyframing

### Fixed

- Slides are marked as 'current' as soon as the top right or bottom right corner is visible.
- Each slide has a maxwidth, preventing the slide size to change depending on the contents. Whatever you put inside (like long words) will not break the slide.
- The space before the first and after the last slide will now consider the space between slides to represent it more accuratly

## [v1.2.0] - 2021-07-26

### Fixed

- Changed the way how the space before the first and after the last slide is calculated. This should fix a problem, where the first slide was actually smaller, than the following slides.
- First bullet not being active on page load
- When having a space before the first slide, hitting the right arrow key will now move the slider to the first slide, rather than by the width of any slide which could lead to the second slide being shown in some cases
- Hitting numbers form 1-9 or clicking bullets will now select the correct bullet
- The slide-sizing mixin should now work as expected in any situation and with any possible combination of parameters

### Added

- A class `current` is being applied to every slide that is fully visible in the viewport
- The previous siblings of the first slide, that is marked as current will have classes applied to them, in relation to their distance to the current element: `previous--1` (closest to current), `previous--2`, `previous--3`. You may select the next siblings via CSS (`[data-a11y-slider-slide] + [data-a11y-slider-slide] {}`)

### Changed

- Current bullet should be more reliable in case not all bullets are visible
- The default space between slides is now `0`

## [v1.1.2] - 2021-07-07

- Spacing for first and last child in case slider has a breakpoint showing exactly 1 slide

## [v1.1.1] - 2021-07-02

### Fixed

- Tests

## [v1.1.0] - 2021-07-02

### Fixed

- slide spacer visible in some cases, not properly sized
- showing only 1 slide and have spacing before the first and after the last slide caused the slide to be larger than possibly visible on screen
- `@mixin slide-sizing` setting param `$spacing` to `0` will no longer crash the slider

### Changed

- `@mixin slide-sizing` has a new parameter `$alignment` to control `scroll-snap-align` (current slide position left, centered, right)
- arrows that are disabled have no pointer events
- Hide arrows and pagination if they are not needed in case we see all slides that are in the slider
- if only one bullet in pagination is visible, hide pagination
- Last bullet will be active if last slide is visible

### Added

- Event listener on `window` resize to update visibility and enabled/disabled state of buttons based on window size
- Method `Next`
- Method `Prev`
- Method `GoTo`

## [v1.0.5] - 2021-06-24

### Changed

- Updated readme

## [v1.0.4] - 2021-06-24

### Changed

- Updated readme

## [v1.0.3] - 2021-06-22

### Added

- Link to example page in readme

### Removed

- Example SCSS

## [v1.0.2] - 2021-06-22

### Removed

- Mutation observer to improve performance (did not use that anyways)

### Changed

- Minor styling tweaks
- Updated readme
- Improved mixin `slide-sizing` to have more possible styling options

## [v1.0.1] - 2021-06-22

### Changed

- Upgraded node modules
- Split up tests into test suites

### Fixed

- gitlab pipeline

## [v1.0.0] - 2021-06-21

### Changed

- Major release

## [v0.0.26] - 2021-06-21

### Fixed

- changelog version

## [v0.0.25] - 2021-06-19

### Added

- Screenshot
- Autoplay (parameter `autoplay`)
- parameter `slideBy` enables the slider to slide more than 1 slide with arrows or autoplay

### Changed

- Readme
- Minor styling

## [v0.0.24] - 2021-06-19

### Changed

- Minor styling tweaks

## [v0.0.23] - 2021-06-19

### Added

- Example styles

## [v0.0.22] - 2021-06-19

### Changed

- Minor styling tweaks

## [v0.0.21] - 2021-06-19

### Changed

- Minor styling tweaks

## [v0.0.20] - 2021-06-19

- Moved `@wanjapflueger/a11y-button` and `smoothscroll-polyfill` to dependencies

## [v0.0.19] - 2021-06-19

### Fixed

- Typo

### Changed

- Added information about `@mixin slide-sizing` and `$space-between-slides` to the readme

## [v0.0.18] - 2021-06-19

### Added

- smoothscroll-polyfill
- Arrows
- Pagination

### Changed

- Destroying a slider will just clone the original Node (as provided in `SliderParameters` on method call `Create()`) and insert it into the DOM unchanged
- Updated test
- Updated Readme

### Fixed

- Position of slider in DOM on method Create

## [v0.0.17] - 2021-05-24

### Added

- Added `require` to [README.md#init](README.md#init) in readme since some people do use that syntax

### Changed

- Updated readme

## [v0.0.16]

### Added

- Caption element
- GET elements.slides
- Selector `[data-a11y-slider-slides]` on slides unordered list

### Change

- Updated tests
- Updated readme

## [v0.0.13]

### Changed

- Use private variables in class
- Remove ES6 constructor
- Updated readme
- Updated tests

## [v0.0.10]

### Added

- Link to changelog in readme

### Changed

- `npm run build` does not generate CSS files but SCSS files
- Updated readme

### Removed

- `node-sass` since we do not need CSS but SCSS

## [v0.0.9]

### Added

- Parameters on `SliderInterface`: `arrows`, `pagination`

### Changed

- Updated Readme

## [v0.0.8]

### Changed

- GET `slider` changed to GET `elements`
- Updated tests
- Renamed param `el` to `list` on `SliderInterface`

## [v0.0.6]

### Changed

- imrpoved testing

## [v0.0.5]

### Added

- Dummy SCSS with stylelint

### Changed

- Class uses constructor
- Updated Tests
- Updated Readme

## [v0.0.1]

### Added

- NPM test release
- Initial release

[unreleased]: https://gitlab.com/wanjapflueger/a11y-slider
[v2.3.3]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v2.3.2...v2.3.3
[v2.3.2]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v2.3.1...v2.3.2
[v2.3.1]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v2.3.0...v2.3.1
[v2.3.0]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v2.2.0...v2.3.0
[v2.2.0]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v2.1.0...v2.2.0
[v2.1.0]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v2.0.4...v2.1.0
[v2.0.4]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v2.0.3...v2.0.4
[v2.0.3]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v2.0.2...v2.0.3
[v2.0.2]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v2.0.1...v2.0.2
[v2.0.1]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v2.0.0...v2.0.1
[v2.0.0]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.7.0...v2.0.0
[v1.7.0]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.6.4...v1.7.0
[v1.6.4]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.6.3...v1.6.4
[v1.6.3]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.6.2...v1.6.3
[v1.6.2]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.6.1...v1.6.2
[v1.6.1]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.6.0...v1.6.1
[v1.6.0]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.5.2...v1.6.0
[v1.5.2]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.5.1...v1.5.2
[v1.5.1]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.5.0...v1.5.1
[v1.5.0]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.4.3...v1.5.0
[v1.4.3]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.4.2...v1.4.3
[v1.4.2]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.4.1...v1.4.2
[v1.4.1]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.4.0...v1.4.1
[v1.4.0]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.3.1...v1.4.0
[v1.3.1]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.3.0...v1.3.1
[v1.3.0]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.2.0...v1.3.0
[v1.2.0]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.1.2...v1.2.0
[v1.1.2]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.1.1...v1.1.2
[v1.1.1]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.1.0...v1.1.1
[v1.1.1]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.1.0...v1.1.1
[v1.1.0]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.0.5...v1.1.0
[v1.0.5]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.0.4...v1.0.5
[v1.0.4]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.0.3...v1.0.4
[v1.0.3]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.0.2...v1.0.3
[v1.0.2]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.0.1...v1.0.2
[v1.0.1]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v1.0.0...v1.0.1
[v1.0.0]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.26...v1.0.0
[v0.0.26]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.25...v0.0.26
[v0.0.25]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.24...v0.0.25
[v0.0.24]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.23...v0.0.24
[v0.0.23]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.22...v0.0.23
[v0.0.22]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.21...v0.0.22
[v0.0.21]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.20...v0.0.21
[v0.0.20]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.19...v0.0.20
[v0.0.19]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.18...v0.0.19
[v0.0.18]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.17...v0.0.18
[v0.0.17]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.16...v0.0.17
[v0.0.16]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.10...v0.0.16
[v0.0.10]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.9...v0.0.10
[v0.0.9]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.8...v0.0.9
[v0.0.8]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.6...v0.0.8
[v0.0.6]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.5...v0.0.6
[v0.0.5]: https://gitlab.com/wanjapflueger/a11y-slider/-/compare/v0.0.1...v0.0.5
[v0.0.1]: https://gitlab.com/wanjapflueger/a11y-slider/-/tree/v0.0.1

# a11y-slider

A slider carousel that is fully accessible. It has no design or theme applied. That way you can be certain the slider is usable and accessible and you can apply your own styles.

[![Pipeline status](https://gitlab.com/wanjapflueger/a11y-slider/badges/master/pipeline.svg)](https://gitlab.com/wanjapflueger/a11y-slider/-/pipelines)
![npm](https://img.shields.io/npm/v/@wanjapflueger/a11y-slider)
![npm bundle size](https://img.shields.io/bundlephobia/min/@wanjapflueger/a11y-slider)

![a11y-slider screenshot](https://gitlab.com/wanjapflueger/a11y-slider/-/raw/master/screenshot.jpg)

_\*Yes I know that the slider in the screenshot above has some [example styles](#example) applied..._

---

## Table of contents

<!-- TOC -->

- [a11y-slider](#a11y-slider)
  - [Table of contents](#table-of-contents)
  - [Requirements](#requirements)
    - [Browser support](#browser-support)
  - [Changelog](#changelog)
  - [Install](#install)
  - [Demo](#demo)
  - [Usage](#usage)
  - [Methods](#methods)
    - [Method Create](#method-create)
      - [Parameters for method create](#parameters-for-method-create)
        - [slides](#slides)
        - [arrows](#arrows)
        - [autoplay](#autoplay)
        - [loop](#loop)
        - [caption](#caption)
        - [lang](#lang)
        - [pagination](#pagination)
        - [slideBy](#slideby)
        - [syncWith](#syncwith)
        - [onMoveStart](#onmovestart)
        - [onMoveEnd](#onmoveend)
        - [onCreated](#oncreated)
        - [onDestroyed](#ondestroyed)
        - [onUpdate](#onupdate)
      - [Method Destroy](#method-destroy)
        - [Breakpoints](#breakpoints)
      - [Method Prev](#method-prev)
      - [Method Next](#method-next)
      - [Method GoTo](#method-goto)
    - [GET](#get)
      - [Get elements](#get-elements)
  - [Styles](#styles)
    - [Import](#import)
    - [Space between slides](#space-between-slides)
    - [Hide every nth bullet](#hide-every-nth-bullet)
    - [Slides per view](#slides-per-view)
  - [Tips and Tricks](#tips-and-tricks)
    - [box-shadow](#box-shadow)
  - [Keyboard support](#keyboard-support)
  - [Dataset attributes](#dataset-attributes)
  - [Issues](#issues)
    - [Event Listeners do not work](#event-listeners-do-not-work)
  - [Accessibility Compliance Report](#accessibility-compliance-report)

<!-- /TOC -->

---

## Requirements

- You need to be able to <a target="_blank" href="https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/import">import</a> or <a target="_blank" href="https://nodejs.org/en/knowledge/getting-started/what-is-require/">require</a> _JavaScript_ files
- You need to be able to <a target="_blank" href="https://sass-lang.com/documentation/at-rules/import">import</a> _SCSS_ with [SASS] (you can do that with _SCSS_ or _JavaScript_/_TypeScript_).

### Browser support

<details>
  <summary>Show list of supported browsers</summary>

- Chrome >= 84
- Firefox >= 63
- Edge >= 84
- Opera >= 73
- Safari >= 14

</details>

## Changelog

See [changelog.md](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/changelog.md).

## Install

```sh
npm i @wanjapflueger/a11y-slider
```

## Demo

[Visit demo page](https://example.barrierefrei.kiwi/a11y-slider/)

## Usage

Import class `Slider`:

```js
import { Slider } from '@wanjapflueger/a11y-slider';
```

Create a new instance:

```js
const mySlider = new Slider();
mySlider.Create({
  // options
});
```

## Methods

### Method Create

1. Create any reference element containing child elements. The contents of the each child element will be a single slide.

   ```html
   <div id="my-list">
     <div>1</div>
     <div>2</div>
     ...
   </div>
   ```

1. Assign that reference element with _JavaScript_ to a variable, initiate a new instance of `Slider` and call _method_ `Create`.

   ```js
   import { Slider } from '@wanjapflueger/a11y-slider';

   const mySlider = new Slider();
   mySlider.Create({
     slides: document.getElementById('my-list'),
   });
   ```

#### Parameters for method create

See _interface_ `SliderParameters` in [src/index.ts](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/src/index.ts).

| Parameter     | Required | Type                                                                                             | Default value                                            | Read more            |
| ------------- | -------- | ------------------------------------------------------------------------------------------------ | -------------------------------------------------------- | -------------------- |
| `slides`      | ✅ Yes   | `HTMLElement`                                                                                    | –                                                        | [More](#slides)      |
| `arrows`      | ❌ No    | `boolean` or [`Arrows`](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/src/index.ts) | `false`                                                  | [More](#arrows)      |
| `autoplay`    | ❌ No    | `number`                                                                                         | `undefined`                                              | [More](#autoplay)    |
| `loop`        | ❌ No    | `boolean`                                                                                        | `false`                                                  | [More](#loop)        |
| `caption`     | ❌ No    | `string`                                                                                         | `'Slider'`                                               | [More](#caption)     |
| `lang`        | ❌ No    | `string`                                                                                         | `document.documentElement.lang` (the documents language) | [More](#lang)        |
| `pagination`  | ❌ No    | `boolean` or `Pagination`                                                                        | `false`                                                  | [More](#pagination)  |
| `slideBy`     | ❌ No    | `number`                                                                                         | `1`                                                      | [More](#slideBy)     |
| `syncWith`    | ❌ No    | `Slider`                                                                                         | `undefined`                                              | [More](#syncWith)    |
| `onMoveStart` | ❌ No    | `(status: Status) => {}`                                                                         | `undefined`                                              | [More](#onMoveStart) |
| `onMoveEnd`   | ❌ No    | `(status: Status) => {}`                                                                         | `undefined`                                              | [More](#onMoveEnd)   |
| `onCreated`   | ❌ No    | `() => {}`                                                                                       | `undefined`                                              | [More](#onCreated)   |
| `onDestroyed` | ❌ No    | `() => {}`                                                                                       | `undefined`                                              | [More](#onDestroyed) |
| `onUpdate`    | ❌ No    | `(status: Status) => {}`                                                                         | `undefined`                                              | [More](#onUpdate)    |

##### slides

<details>
  <summary>Read about <code>slides</code></summary>

Any `HTMLElement` containing child elements (`HTMLUListElement` in the example below). Of each child element (`HTMLLIElement` in the example below) the `innerHTML` will be placed inside a slide. This means the `HTMLUListElement` and `HTMLLIElement[]` will not be transferred to the slider, only the `HTMLDivElement[]` will, because they are inside the children of the `HTMLUListElement`.

Example:

```html
<ul>
  <li><div class="item">A</div></li>
  <li><div class="item">B</div></li>
  <li><div class="item">C</div></li>
</ul>
```

```js
const slider = new Slider();
slider.Create({
  slides: document.querySelector('ul'),
});
```

</details>

##### arrows

<details>
  <summary>Read about <code>arrows</code></summary>

A slider has two arrow-buttons. The arrow buttons let the user navigate to the previous or next slide.

Using default settings with `arrows: true` will create 2 `HTMLButtonElement`s elements with english labels.

```js
const slider = new Slider();
slider.Create({
  // ...
  arrows: true,
});
```

You can easily overwrite the default labels and icons on the arrows using custom text:

```js
const slider = new Slider();
slider.Create({
  // ...
  arrows: {
    label: 'Slider controls',
    prev: {
      label: 'Previous',
      icon: '←',
    },
    next: {
      label: 'Next',
      icon: '→',
    },
  },
});
```

Custom arrows (with existing `HTMLElement`s, do not use a `HTMLButtonElement` since the slider creates a `HTMLButtonElement` for you):

```html
<span class="next">Next slide</span> <span class="prev">Previous slide</span>
```

```js
const slider = new Slider();
slider.Create({
  // ...
  arrows: {
    label: 'Slider controls',
    prev: {
      label: document.querySelector('.prev').innerText,
      icon: document.querySelector('.prev'),
    },
    next: {
      label: document.querySelector('.next').innerText,
      icon: document.querySelector('.next'),
    },
  },
});
```

See also:

- _interface_ [`Arrows`](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/src/index.ts)
- _interface_ [`Arrow`](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/src/index.ts)

</details>

##### autoplay

<details>
  <summary>Read about <code>autoplay</code></summary>

Autoplay speed in milliseconds.

Example:

```js
const slider = new Slider();
slider.Create({
  // ...
  autoplay: 4000 | undefined,
});
```

</details>

##### loop

<details>
  <summary>Read about <code>loop</code></summary>

Go from the first to the last slide and the other way around.

Example:

```js
const slider = new Slider();
slider.Create({
  // ...
  loop: true | false,
});
```

</details>

##### caption

<details>
  <summary>Read about <code>caption</code></summary>

Slider caption for screen readers and bots. If empty will use:

- the `[aria-label]` attribute text on [slides](#slides) or
- the `innerText` of any `Element` that is referenced with `[aria-labelledby]` or
- the `[title]` attribute text on [slides](#slides).

Example:

```js
const slider = new Slider();
slider.Create({
  // ...
  caption: 'My Slider' | undefined,
});
```

</details>

##### lang

<details>
  <summary>Read about <code>lang</code></summary>

Language Code <a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes" target="_blanK">ISO 639-1</a>. This may allow some screen readers to read the correct language for the controls. It is not guaranteed that screen readers support this behavior.

The a11y-slider does not come with a translation file for any controls.

Example:

```js
const slider = new Slider();
slider.Create({
  // ...
  lang: 'de' | 'es' | 'en' | undefined,
});
```

</details>

##### pagination

<details>
  <summary>Read about <code>pagination</code></summary>
Pagination configuration. You can change the label for the pagination element. You can change the `innerHTML` of the `HTMLButtonElement[]` elements that are the paginations bullets. Each bullet displays a number by default (starting with `'1'`). Each Bullet will always have the current index as text for screen readers and bots.

```html
<span class="bullet">⦿</span>
```

```js
const slider = new Slider();
slider.Create({
  // ...
  pagination:
    true |
    false |
    {
      icon: document.querySelector('.bullet') | '⦿' | undefined,
      label: 'Slider pagination' | undefined,
    },
});
```

See also:

- _interface_ [`Pagination`](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/src/index.ts)

</details>

##### slideBy

<details>
  <summary>Read about <code>slideBy</code></summary>

Number of slides to scroll when using prev/next arrows, arrow keys or autoplay.

Example:

```js
const slider = new Slider();
slider.Create({
  // ...
  slideBy: 2 | undefined,
});
```

</details>

##### syncWith

<details>
  <summary>Read about <code>syncWith</code></summary>

Sync a slider with other sliders.

Example:

```js
const sliderA = new Slider();
const sliderB = new Slider();
const sliderC = new Slider();
sliderA.Create({
  // ...
  syncWith: [sliderB, sliderC],
});
sliderB.Create({
  // ...
  syncWith: [sliderA, sliderC],
});
sliderC.Create({
  // ...
  syncWith: [sliderA, sliderB],
});
```

See also:

- _interface_ [`Slider`](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/src/index.ts)

  </details>

##### onMoveStart

<details>
  <summary>Read about <code>onMoveStart</code></summary>

Callback function once the sliders movement starts.

Example:

```js
const slider = new Slider()
slider.Create({
  // ...
  onMoveStart: (status) => {
    console.log('Slider has started moving', status)
  } | undefined
})
```

See also:

- _interface_ [`Status`](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/src/index.ts)

</details>

##### onMoveEnd

<details>
  <summary>Read about <code>onMoveEnd</code></summary>

Callback function once the sliders movement stops.

Example:

```js
const slider = new Slider()
slider.Create({
  // ...
  onMoveEnd: (status) => {
    console.log('Slider has stopped moving', status)
  } | undefined
})
```

See also:

- _interface_ [`Status`](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/src/index.ts)

</details>

##### onCreated

<details>
  <summary>Read about <code>onCreated</code></summary>

Callback function after the slider was created. Function is called once, when the slider is successfully [created](#method-create). Use this to apply event listeners to elements in any slide ([read more](#event-listener-do-not-work)).

Example:

```js
const slider = new Slider()
slider.Create({
  // ...
  onCreated: () => {
    console.log('Slider was created')
  } | undefined
})
```

</details>

##### onDestroyed

<details>
  <summary>Read about <code>onDestroyed</code></summary>

Callback function after the slider was destroyed. Function is called once, when the slider is successfully [destroyed](#method-destroy).

Example:

```js
const slider = new Slider()
slider.Create({
  // ...
  onDestroyed: () => {
    console.log('Slider was destroyed')
  } | undefined
})
```

</details>

##### onUpdate

<details>
  <summary>Read about <code>onUpdate</code></summary>

Callback function on update. Update is called, whenever the slider changes or moves.

Example:

```js
const slider = new Slider()
slider.Create({
  // ...
  onUpdate: (status) => {
    console.log('Slider is moving or updating', status)
  } | undefined
})
```

See also:

- _interface_ [`Status`](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/src/index.ts)

</details>

#### Method Destroy

Destroy the slider.

```js
mySlider.Destroy();
```

Once destroyed you can call [Method Create](#method-create) again at any time, for example on a window resize event.

##### Breakpoints

To destroy the slider for a specific breakpoint, whilst maintaining a solid performance, one could use the following code. Instead of the Vanilla JavaScript helper function `myEfficientFn`, you could use [throttle-debounce](https://www.npmjs.com/package/throttle-debounce).

<details>
  <summary>Expand code snippet</summary>

```js
myEfficientFn(
  window,
  'resize',
  () => {
    if (window.innerWidth >= 1024) {
      mySlider.Destroy();
    } else {
      mySlider.Create();
    }
  },
  100,
  true,
);
```

</details>

_myEfficientFn.js_

<details>
  <summary>Expand code snippet</summary>

```js
/**
 * Very efficient function with debounce and throttle events
 * @param {HTMLElement} target Element to apply event listener to
 * @param {EventListener} eventListener EventListener
 * @param {() => void} callbackFunction Any callback function
 * @param {number} repeatAfter Wait for miliseconds after each call
 * @param {boolean=} callOnLoad Call func when this function is called
 * @returns {void}
 * @example
 *   myEfficientFn(window, 'scroll', () => { return console.log('hello'); }, 100, true);
 */
const myEfficientFn = (
  target,
  eventListener,
  callbackFunction,
  repeatAfter,
  callOnLoad,
) => {
  /**
   * Throttle function
   * @see {@link https://codeburst.io/throttling-and-debouncing-in-javascript-b01cad5c8edf}
   * @param {() => void} f Callback function
   * @param {number} w Wait for miliseconds until next execution
   * @returns {() => void} Calls callback function
   * @example
   *   window.addEventListener('scroll', throttle(() => { return console.log('hello') }, 100));
   */
  const throttle = (f, w) => {
    /** @type {boolean} Is throtteling */
    let t;

    return () => {
      /** @type {*} Arguments */
      const a = arguments;

      /** @type {*} Context */
      const c = this;

      if (!t) {
        f.apply(c, a);
        t = true;
        setTimeout(() => (t = false), w);
      }
    };
  };

  /**
   * Returns a function, that, as long as it continues to be invoked, will not be triggered. The function will be called after it stops being called for N milliseconds. If `immediate` is passed, trigger the function on the leading edge, instead of the trailing.
   * @see {@link https://davidwalsh.name/javascript-debounce-function}
   * @param {() => void} f Callback function
   * @param {number} w Wait for miliseconds until next execution
   * @param {boolean} i Do not wait
   * @returns {() => void} func
   * @example
   *   window.addEventListener('scroll', debounce(() => { console.log('hello'); }, 100));
   */
  const debounce = (f, w, i) => {
    /** @type {Number|null} Timeout */
    let t;

    return () => {
      /** @type {*} Arguments */
      const a = arguments;

      /** @type {*} Context */
      const c = this;

      const later = () => {
        t = null;
        if (!i) f.apply(c, a);
      };

      /** @type {Boolean} Call now */
      const n = i && !t;
      clearTimeout(t);
      t = setTimeout(later, w);
      if (n) f.apply(c, a);
    };
  };

  // Call on load
  if (callOnLoad) {
    callbackFunction.call();
  }

  // Throttle
  target.addEventListener(
    eventListener,
    throttle(() => {
      return callbackFunction.call();
    }, repeatAfter),
  );

  // Debounce
  target.addEventListener(
    eventListener,
    debounce(() => {
      callbackFunction.call();
    }, repeatAfter),
  );
};
```

</details>

#### Method Prev

Go to previous slide.

```js
mySlider.Prev();
```

#### Method Next

Go to next slide.

```js
mySlider.Next();
```

#### Method GoTo

Go to a specific slide.

```js
mySlider.GoTo(3); // go to slide 3
```

### GET

#### Get elements

Get all the sliders HTML elements.

```js
/** Previous arrow */
mySlider.elements.prev;

/** Next arrow */
mySlider.elements.next;

/** Wrapper for {@link prev} and {@link next} */
mySlider.elements.arrows;

/** The sliders caption */
mySlider.elements.caption;

/** Slides */
mySlider.elements.items;

/** Container containing {@link items} */
mySlider.elements.list;

/** Pagination containing {@link bullets} */
mySlider.elements.pagination;

/** Bullets */
mySlider.elements.bullets;

/** This element is a direct child of the slides container. It is used to create the space before the first slide. */
mySlider.elements.spacerBefore;

/** This element is a direct child of the slides container. It is used to create the space after the last slide. */
mySlider.elements.spacerAfter;
```

The slider itself can be accessed with:

```js
/** The slider element */
mySlider.theSlider;
```

## Styles

The slider does not come with any design or theme applied. It has only required functional styles. You may however apply some [example styles](#example-styles).

### Import

Some functional CSS is required to make the slider work. Make sure to import the _SCSS_ from the node module.

Via _SCSS_

```scss
@import 'path-to/node_modules/@wanjapflueger/a11y-slider/lib/index';
```

Via _JavaScript_

```js
import 'path-to/node_modules/@wanjapflueger/a11y-slider/lib/index.scss';
```

### Space between slides

Overwrite the default value for `$space-between-slides` from [src/partials/slide/\_index.scss](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/src/partials/slide/_index.scss) globally **before you import the SCSS**.

```scss
$space-between-slides: 80px;

@import 'path-to/node_modules/@wanjapflueger/a11y-slider/lib/index';
```

If you do not want to set this value globally but per slider, continue reading [Slides per view](#slides-per-view).

### Hide every nth bullet

A single bullet exists for each slide. However if you have lots of slides this will look silly. You can hide some of those bullets like shown in the example below.

<details>
  <summary>Show code example</summary>

```scss
[data-a11y-slider] [data-a11y-slider-pagination] > nav > ul > li {
  // Show only every fourth bullet (1, 5, 9 etc.) for tablet
  &:not(:nth-child(4n + 1)) {
    @media (max-width: 767px) {
      display: none;
    }
  }

  // Show only every second bullet (1, 3, 5, etc.) for tablet
  &:not(:nth-child(2n + 1)) {
    @media (min-width: 768px) and (max-width: 1024px) {
      display: none;
    }
  }
}
```

</details>

### Slides per view

Use `@mixin slide-sizing` from [src/partials/slide/\_index.scss](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/src/partials/slide/_index.scss) to define how many slides should be visible for each breakpoint.

<details>
  <summary>Show code example</summary>

```scss
[data-a11y-slider] {
  @include slide-sizing(
    1
  ); // Mobile: show 1 slide, space between slides is $space-between-slides

  @media (min-width: 1280px) {
    @include slide-sizing(
      2.5,
      80
    ); // Desktop: show 2 and a half slides, space between slides is 80px
  }
}
```

</details>

**Pro Tip:** Even if you display only one slide with `slide-sizing(1)` you can still provide a grid gap.

## Tips and Tricks

### box-shadow

When using `box-shadow`, add a `margin` to the slides, to avoid overflow clipping:

```scss
[data-a11y-slider-slide] {
  margin: 2em 0; // set margin according to the shadows dimensions
}
```

## Keyboard support

In [src/partials/keys.ts](https://gitlab.com/wanjapflueger/a11y-slider/-/blob/master/src/partials/keys.ts) several keys have been assigned a function. The keyboard support starts when a slider is active. A slider is active when:

- the user hovers the slider
- the focus is on the slider (`[data-a11y-slider]`)
- any child element inside the slider has the focus (`[data-a11y-slider]`)

The slider will have `[aria-current="true"]` while active.

| Icon | Key          | Description    |
| ---- | ------------ | -------------- |
| →    | `ArrowRight` | Next slide     |
| ←    | `ArrowLeft`  | Previous slide |
| 1    | `Digit1`     | Slide 1        |
| 2    | `Digit2`     | Slide 2        |
| 3    | `Digit3`     | Slide 3        |
| 4    | `Digit4`     | Slide 4        |
| 5    | `Digit5`     | Slide 5        |
| 6    | `Digit6`     | Slide 6        |
| 7    | `Digit7`     | Slide 7        |
| 8    | `Digit8`     | Slide 8        |
| 9    | `Digit9`     | Slide 9        |

## Dataset attributes

You may overwrite some _JavaScript_ [parameters](#parameters) with the following HTML attributes. Set these attributes on your [reference element](#usage).

- [autoplay](#autoplay)

  ```html
  <div id="my-list" data-a11y-slider-autoplay="4000">...</div>
  ```

- [lang](#lang)

  ```html
  <div id="my-list" data-a11y-slider-lang="de">...</div>
  ```

- [slideBy](#slideby)

  ```html
  <div id="my-list" data-a11y-slider-slide-by="2">...</div>
  ```

## Issues

### Event Listeners do not work

Issue: [https://gitlab.com/wanjapflueger/a11y-slider/-/issues/1](https://gitlab.com/wanjapflueger/a11y-slider/-/issues/1)

As stated in [How to copy a DOM node with event listeners?](https://stackoverflow.com/a/15411683), event listeners applied to elements within the slider cannot be copied when creating a new slider instance.

To workaround this issue, apply any event listeners in a callback function, that can be passed with the [parameter `onCreated`](#parameters-on-created).

<details>
  <summary>Show code example</summary>

```js
slider.Create({
  // ...
  onCreated: () => {
    const buttons = document.querySelectorAll('button');
    buttons.forEach((button) => {
      button.addEventListener('click', () => {
        console.log('clicked on a button inside a slide');
      });
    });
  },
  // ...
});
```

</details>

## Accessibility Compliance Report

[WCAG](https://www.w3.org/WAI/WCAG21/quickref/) Level: **AAA** [^1]

| Browser              | Platform      | Screen reader                                                              | Passed |
| -------------------- | ------------- | -------------------------------------------------------------------------- | ------ |
| Chrome 90.0.4430.212 | MacOS 10.15.7 | [VoiceOver](https://www.apple.com/de/accessibility/vision/)                | ✅     |
| Chrome 90.0.4430.210 | Android 10    | [Talkback](https://support.google.com/accessibility/android#topic=6007234) | ✅     |

---

[^1]: This information refers only to the technical aspects of the component, not to the design or the editorial handling of any content.

import { Slider } from '..';
import { SliderSlidesIsLastSlide } from './slides';
import { SliderMove, SliderIsActive } from './slider';

/**
 * Slide automatically
 * @param that Slider reference
 * @example
 *   SliderAutoplayDoSlide(slider)
 */
const SliderAutoplayDoSlide = (that: Slider): void => {
  if (SliderSlidesIsLastSlide(that)) {
    SliderMove(that, 'to', 1);
  } else {
    SliderMove(that, 'by', that.parameters?.slideBy || 1);
  }
};

/**
 * Is Autoplay currently running?
 * @param  that Slider reference
 * @returns `true` if running
 * @example
 *   const running = SliderAutoplayIsRunning(slider)
 */
const SliderAutoplayIsRunning = (that: Slider): boolean => {
  return (
    (that.theSlider?.dataset.a11ySliderAutoplayRunning &&
      that.theSlider?.dataset.a11ySliderAutoplayRunning === 'true') ||
    false
  );
};

/**
 * To avoid overlaypping iterations a dataset value is applied to the slider that indicates, that this sliders autoplay functionality has already been initiated.
 * @param that Slider reference
 * @returns `true` if autoplay has been initiated
 * @example
 *   const autoplayInitialized = SliderAutoplayIsInitiated(slider)
 */
const SliderAutoplayIsInitiated = (that: Slider): boolean => {
  return (
    (that.theSlider?.dataset.a11ySliderAutoplayCheck &&
      that.theSlider?.dataset.a11ySliderAutoplayCheck === 'true') ||
    false
  );
};

/**
 * Start or stop autoplay depending on slider state
 * @param that Slider reference
 * @example
 *   SliderAutoplayUpdate(slider)
 */
const SliderAutoplayUpdate = (that: Slider): void => {
  if (!that.parameters) return;

  // Check for current state after timeout
  setTimeout(() => {
    if (!that.parameters) return;

    if (
      that.parameters.autoplay &&
      typeof that.parameters.autoplay === 'number'
    ) {
      if (SliderIsActive(that)) {
        // Autoplay is disabled when the slider has focus
        SliderAutoplayStop(that);
      } else {
        SliderAutoplayStart(that);
      }
    } else {
      SliderAutoplayStop(that);
    }

    // Do the slide
    if (SliderAutoplayIsRunning(that)) {
      SliderAutoplayDoSlide(that);
    }

    SliderAutoplayUpdate(that);
  }, that.parameters.autoplay);
};

/**
 * Start slider autoplay
 * @param that Slider reference
 * @example
 *   SliderAutoplayStart(slider)
 */
const SliderAutoplayStart = (that: Slider): void => {
  if (that.theSlider) that.theSlider.dataset.a11ySliderAutoplayRunning = 'true';
};

/**
 * Stop slider autoplay
 * @param that Slider reference
 * @example
 *   SliderAutoplayStop(slider)
 */
const SliderAutoplayStop = (that: Slider): void => {
  if (that.theSlider)
    that.theSlider.dataset.a11ySliderAutoplayRunning = 'false';
};

/**
 * Slider Autoplay controller. Will only execute once but can be called repeatedly.
 * @param that Slider reference
 * @example
 *   SliderAutoplayController(slider)
 */
export const SliderAutoplayController = (that: Slider): void => {
  if (!that.theSlider || SliderAutoplayIsInitiated(that)) return;

  // Initiate autoplay on slider once
  SliderAutoplayUpdate(that);
  that.theSlider.dataset.a11ySliderAutoplayCheck = 'true';
};

import { Slider } from '../..';

/**
 * TypeScript throws an error `error TS2339: Property 'scrollMargin' does not exist on type 'CSSStyleDeclaration'` so I constructed a workaround to access that value by creating a new Interface
 */
export interface FixedCSSStyleDeclaration extends CSSStyleDeclaration {
  /** Fixed scroll margin value  */
  scrollMargin: string;
}

/**
 * Get all slides
 * @param that Slider reference
 * @returns All slides
 * @example
 *   const items = SliderSlideGetAll(slider)
 */
export const SliderSlideGetAll = (
  that: Slider,
): NodeListOf<HTMLLIElement> | undefined =>
  that.theSlider?.querySelectorAll(
    '[data-a11y-slider-slide]:not([data-a11y-slider-slide-spacer]',
  );

/**
 * Get width of a single slide
 * @param that Slider reference
 * @returns The width of a slide in pixel (each slide is expected to have the same width)
 * @example
 *   const width = SliderSlideGetWidth(slider)
 */
export const SliderSlideGetWidth = (that: Slider): number | undefined => {
  const items = SliderSlideGetAll(that);
  if (items && items.length) {
    return items[0].clientWidth;
  }

  return undefined;
};

/**
 * Get scroll-margin value. This value is the same amongst all slides within the slider. Checkout this link if you don't know what the scroll-margin is: {@link https://developer.mozilla.org/en-US/docs/Web/CSS/scroll-margin}
 * @param that Slider reference
 * @returns The scroll margin value in pixel (`0` = indicates there is no scrollMargin being used).
 * @example
 *   const margin = SliderSlideGetScrollMargin(slider)
 */
export const SliderSlideGetScrollMargin = (that: Slider): number => {
  const slides = SliderSlideGetAll(that);
  if (slides && slides.length) {
    /** Slide from {@link slides} with index > `0` since that particular slide would return `0px` for its computed value. */
    const slide = slides[1];
    if (!slide) return 0;

    const comptStyles = window.getComputedStyle(
      slide,
    ) as FixedCSSStyleDeclaration;
    return parseInt(comptStyles.scrollMargin, 10);
  }

  return 0;
};

/**
 * Create a single slide element from a reference element
 * @param referenceElement Any element that contains the contents of a slide
 * @returns List element
 * @example
 *   Array.from(document.querySelector('ul').children).forEach(child => {
 *     SliderSlideCreate(child as HTMLElement)
 *   })
 */
export const SliderSlideCreate = (
  referenceElement: HTMLElement,
): HTMLLIElement => {
  const el = document.createElement('li');
  el.innerHTML = referenceElement.innerHTML;
  el.setAttribute('data-a11y-slider-slide', '');

  return el;
};

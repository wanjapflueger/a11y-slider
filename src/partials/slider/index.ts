import {
  SliderArrowsBuild,
  SliderArrowsBuildWrapper,
  SliderArrowsOnUpdate,
} from '../arrows';
import {
  SliderSlidesGet,
  SliderSlidesIsLastSlide,
  SliderSlidesGetCurrentPosInPx,
  SliderSlidesIsFirstSlide,
} from '../slides';
import {
  FixedCSSStyleDeclaration,
  SliderSlideGetAll,
  SliderSlideGetScrollMargin,
  SliderSlideGetWidth,
} from '../slide';
import {
  SliderKeysAddEventListener,
  SliderKeysRemoveEventListener,
} from '../keys';
import { SliderPaginationBuild, SliderPaginationOnUpdate } from '../pagination';
import { Slider, SliderParameters, Status } from '../..';
import { SliderCaptionBuild } from '../caption';
import { SliderSlidesBuild } from '../slides';
import { SliderTrackBuild } from '../track';
import { uuid, isElementInViewport } from '../helper';
import { SliderAutoplayController } from '../autoplay';

/**
 * Build the slider
 * @param that Slider reference
 * @returns Slider element
 * @example
 *   SliderSlidesBuildSlider(slider)
 */
export const SliderSlidesBuildSlider = (
  that: Slider,
): HTMLDivElement | undefined => {
  if (!that.id || !that.parameters) return undefined;

  const el = document.createElement('div');
  el.setAttribute('data-a11y-slider', '');
  el.tabIndex = 0; // must be focussable since we use aria-controls on slider controls
  el.id = that.id;

  /** Unique id to connect caption to slides list */
  const captionId = uuid();

  // Create elements
  const caption: HTMLSpanElement | undefined = SliderCaptionBuild(
    that,
    captionId,
  );
  const list: HTMLUListElement | undefined = SliderSlidesBuild(that, captionId);
  const listWrapper: HTMLDivElement | undefined = SliderTrackBuild();
  const arrowsWrapper: HTMLDivElement | undefined = SliderArrowsBuildWrapper();
  let arrows: HTMLDivElement | undefined;
  let pagination: HTMLDivElement | undefined;

  if (that.parameters.arrows) arrows = SliderArrowsBuild(that);
  if (that.parameters.pagination) pagination = SliderPaginationBuild(that);

  // Add elements to slider
  if (caption) el.append(caption);
  el.append(arrowsWrapper);
  arrowsWrapper.append(listWrapper);
  if (list) listWrapper.append(list);
  if (that.parameters.arrows && arrows) arrowsWrapper.append(arrows);
  if (that.parameters.pagination && pagination) el.append(pagination);

  return el;
};

/**
 * Add event listeners to slider
 * @param that Slider reference
 * @returns `true` if event listeners were applied successfully
 * @example
 *   SliderSetEventListener(slider)
 */
const SliderSetEventListener = (that: Slider) => {
  if (!that.theSlider) return false;

  /**
   * Mouse enters {@link Slider.theSlider}
   * @example
   *   element.addEventListener('focusin', SliderOnMouseEnter);
   */
  const SliderOnMouseEnter = () => {
    /** Set active state to this {@link Slider} */
    SliderSetActive(that);
  };

  that.theSlider.addEventListener('focusin', SliderOnMouseEnter);
  that.theSlider.addEventListener('focusout', () => SliderOnMouseLeave(that));
  that.theSlider.addEventListener('mouseenter', SliderOnMouseEnter);
  that.theSlider.addEventListener('mouseleave', () => SliderOnMouseLeave(that));
  SliderOnFocusChild(that);

  return true;
};

/**
 * Find all elements in slider that can receive focus
 * @param that Slider instance
 * @returns Matching elements
 * @example
 *   const items = SliderGetFocussableElements(slider)
 */
const SliderGetFocussableElements = (
  that: Slider,
): NodeListOf<Element> | undefined =>
  that.theSlider?.querySelectorAll(
    'a, button, input, textarea, select, [tabindex]',
  );

/**
 * Set slider to active state
 * @param that Slider reference
 * @example
 *   SliderSetActive(slider)
 */
const SliderSetActive = (that: Slider) => {
  if (
    !that.theSlider?.getAttribute('aria-current') ||
    that.theSlider?.getAttribute('aria-current') !== 'true'
  ) {
    that.theSlider?.setAttribute('aria-current', 'true');

    // Add keyboard support to active slider
    SliderKeysAddEventListener(that);
  }
};

/**
 * Is slider in active state?
 * @param that Slider reference
 * @returns If the slider is currently in an active state
 * @example
 *   const isActive = SliderIsActive(slider)
 */
export const SliderIsActive = (that: Slider) => {
  return that.theSlider?.getAttribute('aria-current') === 'true';
};

/**
 * Set slider to inactive state
 * @param that Slider reference
 * @example
 *   SliderSetInactive(slider)
 */
const SliderSetInactive = (that: Slider) => {
  that.theSlider?.removeAttribute('aria-current');

  // Remove keyboard support for slider
  SliderKeysRemoveEventListener();
};

/**
 * Mouse leaves this {@link Slider.theSlider}
 * @param that Slider reference
 * @example
 *   item.addEventListener('blur', () => SliderOnMouseLeave(slider));
 */
const SliderOnMouseLeave = (that: Slider) => {
  let activeElement: Element | undefined;

  const items = SliderGetFocussableElements(that);

  if (items) {
    for (let i = 0, n = Array.from(items); i < n.length; i++) {
      const item = n[i];
      if (item === document.activeElement) {
        activeElement = item;
        break;
      }
    }
  }

  if (!activeElement) {
    SliderSetInactive(that);
  }
};

/**
 * Mark slides that are currently visible so we can target them with CSS
 * @param that Slider reference
 * @example
 *   SliderMarkVisibleSlides(slider)
 */
const SliderMarkVisibleSlides = (that: Slider) => {
  const currentClassName = 'current';

  if (!that.elements.items) {
    return;
  }

  for (let i = 0, n = Array.from(that.elements.items); i < n.length; i++) {
    const item = n[i];
    item.classList.remove('previous--1');
    item.classList.remove('previous--2');
    item.classList.remove('previous--3');
  }

  for (let i = 0, n = Array.from(that.elements.items); i < n.length; i++) {
    const current = n[i];
    const visible = isElementInViewport(current, that);

    if (visible) {
      current.classList.add(currentClassName);
      const b1 = current.previousElementSibling;
      if (
        b1 &&
        !b1.classList.contains(currentClassName) &&
        !b1.hasAttribute('data-a11y-slider-slide-spacer')
      ) {
        b1.classList.add('previous--1');

        const b2 = b1.previousElementSibling;
        if (
          b2 &&
          !b2.classList.contains(currentClassName) &&
          !b2.hasAttribute('data-a11y-slider-slide-spacer')
        ) {
          b2.classList.add('previous--2');

          const b3 = b2.previousElementSibling;
          if (
            b3 &&
            !b3.classList.contains(currentClassName) &&
            !b3.hasAttribute('data-a11y-slider-slide-spacer')
          ) {
            b3.classList.add('previous--3');
          }
        }
      }
    } else {
      current.classList.remove(currentClassName);
    }
  }

  let hasAVisibleSlide = false;

  for (let i = 0, n = Array.from(that.elements.items); i < n.length; i++) {
    const item = n[i];
    if (item.classList.contains(currentClassName)) {
      hasAVisibleSlide = true;
      break;
    }
  }

  if (!hasAVisibleSlide) {
    // in case no slide is fully visible, mark the first one as visible
    Array.from(that.elements.items)[0].classList.add(currentClassName);
  }
};

/**
 * Create a slider Status
 * @param that Slider reference
 * @returns The slider status
 * @example
 *   SliderStatus(slider)
 */
const SliderStatus = (that: Slider): Status => {
  const status: Status = {
    progress: parseFloat(
      (
        (that.elements.list.scrollLeft /
          (that.elements.list.scrollWidth - that.elements.list.clientWidth)) *
        100
      ).toFixed(2),
    ),
    isFirst: SliderSlidesIsFirstSlide(that),
    isLast: SliderSlidesIsLastSlide(that),
    currentIndex: SliderGetCurrentIndex(that) || 0,
  };

  /** Pass {@link status} to callback function {@link that.parameters.onUpdate} */
  if (that.parameters?.onUpdate) that.parameters.onUpdate(status);

  return status;
};

/**
 * Watch changes on the slider
 * @param that Slider reference
 * @example
 *   SliderWatch(slider)
 */
export const SliderWatch = (that: Slider) => {
  if (!that.theSlider || !that.parameters) return;

  /**
   * Slider is updating
   * @example
   *   update()
   */
  const update = () => {
    SliderArrowsOnUpdate(that);
    SliderAutoplayController(that);
    SliderPaginationOnUpdate(that);
    SliderMarkVisibleSlides(that);
    SliderStatus(that);
  };

  /** Animation keyframe is ticking */
  let sliderScrollTicking = false;
  let windowScrollTicking = false;
  let windowResizeTicking = false;

  // On Load
  update();
  SliderSetEventListener(that);

  /** Use this to set {@link isMoving} */
  let timeout: NodeJS.Timeout;

  /** As long this is `true`, the slider ({@link that}) is moving */
  let isMoving = false;

  // On slider scroll
  SliderSlidesGet(that)?.addEventListener('scroll', () => {
    if (isMoving === false) {
      if (that.parameters?.onMoveStart) {
        that.parameters?.onMoveStart(SliderStatus(that));
      }

      isMoving = true;
    }

    if (!sliderScrollTicking) {
      window.requestAnimationFrame(() => {
        update();

        sliderScrollTicking = false;

        clearTimeout(timeout);
        timeout = setTimeout(() => {
          if (that.parameters?.onMoveEnd) {
            that.parameters?.onMoveEnd(SliderStatus(that));
          }

          if (that.parameters?.syncWith?.length) {
            that.parameters.syncWith.forEach((slider) => {
              const index = SliderStatus(that).currentIndex + 1;
              SliderMove(slider, 'to', index);
            });
          }

          isMoving = false;
        }, 100);
      });

      sliderScrollTicking = true;
    }
  });

  // On window scroll
  window.addEventListener('scroll', () => {
    if (!windowScrollTicking) {
      window.requestAnimationFrame(() => {
        update();
        windowScrollTicking = false;
      });

      windowScrollTicking = true;
    }
  });

  // On window resize
  window.addEventListener('resize', () => {
    if (!windowResizeTicking) {
      window.requestAnimationFrame(() => {
        update();
        windowResizeTicking = false;
      });

      windowResizeTicking = true;
    }
  });
};

/**
 * Move the slider
 * @param that Slider reference
 * @param method Method that is used to move the slider
 * @param multiplier Number of slides to scroll or index of slide in slider to scroll to (1 = first slide)
 * @example
 *   SliderMove(el, 'by', 2) // scroll slider by 2 slides
 *   SliderMove(el, 'to', 2) // scroll slider to slide 2
 */
export const SliderMove = (
  that: Slider,
  method: 'by' | 'to',
  multiplier: number,
) => {
  const slides = SliderSlidesGet(that);
  if (!slides) return;

  /** ScrollTo left value */
  let left: number;

  const isFirst = SliderSlidesIsFirstSlide(that);

  if (method === 'to') {
    const allSlides = SliderSlideGetAll(that);
    const targetSlide = allSlides ? allSlides[multiplier - 1] : undefined;
    if (!targetSlide) return;
    const comptStyle = window.getComputedStyle(
      targetSlide,
    ) as FixedCSSStyleDeclaration;
    const scrollMargin = parseInt(comptStyle.scrollMargin, 10);
    if (scrollMargin > 0) {
      left =
        targetSlide.offsetLeft - scrollMargin !== targetSlide.offsetLeft
          ? targetSlide.offsetLeft - scrollMargin
          : 0;
    } else {
      left = targetSlide.offsetLeft || 0;
    }

    // Scroll to a specific slide or by a number of slides
    slides.scrollTo({
      left,
      behavior: 'smooth',
    });
  } else {
    if (multiplier === 1 && isFirst) {
      /** When having a large space-before we want to avoid that the user has to click twice to go to the second slide. */
      SliderMove(that, 'to', 2);
      return;
    }

    if (
      that.parameters?.loop &&
      multiplier < 0 &&
      SliderSlidesIsFirstSlide(that)
    ) {
      // Loop from first to last item
      left = that.elements.list.scrollWidth; // scroll to last item
    } else if (
      that.parameters?.loop &&
      multiplier > 0 &&
      SliderSlidesIsLastSlide(that)
    ) {
      // Loop from last to first item
      left = 0; // scroll to first item
    } else {
      const width = SliderSlideGetWidth(that) || 0;
      const scrollMargin = SliderSlideGetScrollMargin(that);

      if (scrollMargin > 0) {
        if (scrollMargin > width) {
          /** In case {@link scrollMargin} is greater than {@link width} we have to sum up those values. Otherwise {@link left} will be too little to cause the slider to move.  */
          left = slides.scrollLeft + (width + scrollMargin) * multiplier;
        } else {
          left = slides.scrollLeft + width * multiplier;
        }
      } else {
        const marginLeft = that.elements.items[1]
          ? parseInt(
              window.getComputedStyle(that.elements.items[1]).marginLeft,
              10,
            )
          : undefined;
        if (marginLeft && marginLeft > width) {
          /** In case {@link scrollMargin} is not set but the spaceBetweenSlides is still greater than the {@link width} we need to sup up those values. */
          left = slides.scrollLeft + (width + marginLeft) * multiplier;
        } else {
          left = slides.scrollLeft + width * multiplier;
        }
      }
    }

    // Scroll to a specific slide or by a number of slides
    slides.scrollTo({
      left,
      behavior: 'smooth',
    });
  }
};

/**
 * Listen for FocusEvent on any focussable item inside the slider
 * @param that Slider reference
 * @example
 *   SliderOnFocusChild(slider)
 */
const SliderOnFocusChild = (that: Slider) => {
  if (!that.theSlider) return;

  const items = SliderGetFocussableElements(that);

  if (items && items.length) {
    for (let i = 0, n = Array.from(items); i < n.length; i++) {
      const item = n[i];
      item.addEventListener('blur', () => SliderOnMouseLeave(that));
      item.addEventListener('focus', () => {
        SliderOnFocus(that);
      });
      item.addEventListener('focusout', () => SliderSetInactive(that));
    }
  }
};

/**
 * Handle focus on an element, that is inside a slider element. Focussing on such an element would cause the slider to get active {@link SliderSetActive}.
 * @param that Slider reference
 * @example
 *   item.addEventListener('focus', () => { SliderOnFocus(slider) });
 */
const SliderOnFocus = (that: Slider) => {
  SliderSetActive(that);
};

/**
 * What is the current slider index?
 * @param that Slider reference
 * @returns Current index (`0` = first slide) or `undefined` if index could not be determined
 * @example
 *   const currentSlideIndex = SliderGetCurrentIndex(slider)
 */
export const SliderGetCurrentIndex = (that: Slider): number | undefined => {
  if (!that.theSlider) return undefined;

  let index = 0;
  const items = SliderSlideGetAll(that);

  if (SliderSlidesIsLastSlide(that)) {
    if (items) {
      index = items.length - 1;
    }
  } else {
    const pos = SliderSlidesGetCurrentPosInPx(that);
    if (!pos) return;

    const positions = [];

    if (items) {
      for (let i = 0, n = Array.from(items); i < n.length; i++) {
        const slide = n[i];
        const comptStyle = window.getComputedStyle(
          slide,
        ) as FixedCSSStyleDeclaration;
        const scrollMargin = parseInt(comptStyle.scrollMargin, 10);
        if (scrollMargin > 0) {
          positions.push(slide.offsetLeft - scrollMargin);
        } else {
          positions.push(slide.offsetLeft);
        }
      }
    }

    const closest = positions.reduce((a, b) => {
      return Math.abs(b - pos) < Math.abs(a - pos) ? b : a;
    });

    index = positions.indexOf(closest);
  }

  return index;
};

/**
 * Modify parameters based on data attributes
 * @param parameters Raw parameters
 * @returns Modified parameters
 * @example
 *   const modifiedParameters = SliderModifyParameters(parameters)
 */
export const SliderModifyParameters = (parameters: SliderParameters) => {
  // Set parameter 'lang' if any [data-a11y-slider-lang="x"] is set
  if (parameters.slides.dataset.a11ySliderLang)
    parameters.lang = parameters.slides.dataset.a11ySliderLang;

  // Set parameter 'autoplay' if any [data-a11y-slider-autoplay="x"] is set
  if (parameters.slides.dataset.a11ySliderAutoplay)
    parameters.autoplay = parseInt(
      parameters.slides.dataset.a11ySliderAutoplay,
      10,
    );

  // Set parameter 'slideBy' if any [data-a11y-slider-slide-by="x"] is set
  if (parameters.slides.dataset.a11ySliderSlideBy)
    parameters.slideBy = parseInt(
      parameters.slides.dataset.a11ySliderSlideBy,
      10,
    );

  return parameters;
};

import { Slider } from '../..';
import { SliderSlideCreate } from '../slide';

/**
 * Build slides
 * @param that Slider reference
 * @param captionId Id that is used for labelling
 * @returns Slides
 * @example
 *   SliderSlidesBuild(new Slider(), 'xxx-yyy')
 */
export const SliderSlidesBuild = (
  that: Slider,
  captionId: string,
): HTMLUListElement | undefined => {
  if (!that.parameters) return undefined;
  const el = document.createElement('ul');
  el.setAttribute('data-a11y-slider-slides', '');
  el.setAttribute('aria-labelledby', captionId);

  /** Create an additional list element as a spacer to keep some space before the first slide. */
  const spacerBefore = document.createElement('li');
  spacerBefore.setAttribute('data-a11y-slider-slide', '');
  spacerBefore.setAttribute('data-a11y-slider-slide-spacer', '');

  /** Create an additional list element as a spacer to keep some space after the last slide. */
  const spacerAfter = document.createElement('li');
  spacerAfter.setAttribute('data-a11y-slider-slide', '');
  spacerAfter.setAttribute('data-a11y-slider-slide-spacer', '');

  el.append(spacerBefore);

  for (
    let i = 0, n = Array.from(that.parameters.slides.children);
    i < n.length;
    i++
  ) {
    el.append(SliderSlideCreate(n[i] as HTMLElement));
  }

  el.append(spacerAfter);

  return el;
};

/**
 * Get all slides
 * @param that Slider reference
 * @returns Slides
 * @example
 *   const slides = SliderSlidesGet(slider)
 */
export const SliderSlidesGet = (that: Slider) =>
  that.theSlider?.querySelector(
    '[data-a11y-slider-slides]',
  ) as HTMLUListElement;

/**
 * Get the current slider position (left edge of slides relative to parent)
 * @param that Slider reference
 * @returns Current scroll position in px
 * @example
 *   const pos = SliderSlidesGetCurrentPosInPx(slider)
 */
export const SliderSlidesGetCurrentPosInPx = (that: Slider): number =>
  SliderSlidesGet(that)?.scrollLeft || 0;

/**
 * Is slider showing first item?
 * @param that Slider reference
 * @returns `true` if first item is showing
 * @example
 *   const firstIsShowing = SliderSlidesIsFirstSlide(slider)
 */
export const SliderSlidesIsFirstSlide = (that: Slider): boolean => {
  /** Padding for calculation */
  const padding = 10;

  return that.elements.list.scrollLeft <= padding;
};

/**
 * Is slider showing last item?
 * @param that Slider reference
 * @returns `true` if last item is showing
 * @example
 *   const lastIsShowing = SliderSlidesIsLastSlide(slider)
 */
export const SliderSlidesIsLastSlide = (that: Slider): boolean => {
  /** Safety padding for calculation */
  const padding = 10;

  return (
    that.elements.list.scrollLeft + that.elements.list.clientWidth >=
    that.elements.list.scrollWidth - padding
  );
};

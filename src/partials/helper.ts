import { Slider } from '..';

/**
 * Create a uniuque id
 * @returns A unique ID
 * @example
 *   const id = uuid()
 */
export const uuid = (): string => {
  return Math.random()
    .toString(36)
    .replace('0.', 'a11y-slider-' || '');
};

/**
 * Are the top-right and bottom-right corners of any element visible in viewport?
 * @see {@link https://stackoverflow.com/questions/123999/how-can-i-tell-if-a-dom-element-is-visible-in-the-current-viewport#answer-7557433}
 * @param el Any HTML Element
 * @param that Slider reference
 * @returns True if the top right or bottom right corner is in the viewport
 * @example
 *   const inViewport = isElementInViewport(el, slider)
 */
export const isElementInViewport = (el: HTMLElement, that: Slider): boolean => {
  const rect = el.getBoundingClientRect();
  const w = window.innerWidth || document.documentElement.clientWidth;
  const h = window.innerHeight || document.documentElement.clientHeight;

  const visible =
    rect.top <= h &&
    (rect.top >= 0 || (rect.top < 0 && rect.top * -1 < el.clientHeight)) &&
    rect.left >= 0 &&
    rect.right <= w &&
    rect.right <=
      that.elements.list.getBoundingClientRect().left +
        that.elements.list.offsetWidth;

  return visible;
};

/**
 * Is element visible to the user?
 * @see {@link https://stackoverflow.com/questions/19669786/check-if-element-is-visible-in-dom#answer-41698614}
 * @param el Any HTML Element
 * @returns True if element is visible
 * @example
 *   const elIsVisible = isVisible(el)
 */
export const isVisible = (el: HTMLElement): boolean => {
  if (!el) return false;
  if (el.style && el.style.display === 'none') return false;
  if (el.style && el.style.visibility && el.style.visibility !== 'visible')
    return false;
  if (el.style && el.style.opacity && parseFloat(el.style.opacity) < 0.1)
    return false;
  if (
    el.offsetWidth +
      el.offsetHeight +
      el.getBoundingClientRect().height +
      el.getBoundingClientRect().width ===
    0
  ) {
    return false;
  }
  const elemCenter = {
    x: el.getBoundingClientRect().left + el.offsetWidth / 2,
    y: el.getBoundingClientRect().top + el.offsetHeight / 2,
  };
  if (elemCenter.x < 0) return false;
  if (
    elemCenter.x > (document.documentElement.clientWidth || window.innerWidth)
  )
    return false;
  if (elemCenter.y < 0) return false;
  if (
    elemCenter.y > (document.documentElement.clientHeight || window.innerHeight)
  )
    return false;
  let pointContainer: Node | null | undefined = document.elementFromPoint(
    elemCenter.x,
    elemCenter.y,
  );
  do {
    if (pointContainer === el) return true;
  } while (pointContainer === pointContainer?.parentNode);
  {
    pointContainer = pointContainer?.parentNode;
  }
  return false;
};

/**
 * Find a caption for any element. This function checks several attributes and possibly related elements and extracts a possible caption as string.
 * @param el Any element
 * @returns Caption
 * @example
 *   const caption = findCaption(el)
 */
export const findCaption = (el: HTMLElement): string | undefined => {
  let caption: string | undefined;

  // caption is found in attribute aria-label
  caption = el.getAttribute('aria-label') || undefined;
  if (caption) return caption;

  // caption is found on a referenced element
  const refElementId = el.getAttribute('aria-labelledby');
  if (refElementId) {
    const refElement = document.getElementById(refElementId);
    if (refElement) {
      const text = refElement.innerText;
      if (text) caption = text || undefined;
    }
  }
  if (caption) return caption;

  // caption is found in attribute title
  caption = el.title || undefined;
  if (caption) return caption;

  // no caption could be found
  return undefined;
};

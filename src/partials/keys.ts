import { Slider } from '..';
import { SliderMove } from './slider';

/** Current {@link Slider} reference */
let currentSlider: Slider | undefined;

/** Slider Keyboard Controls */
const SliderKeyboardControls: object = {
  ArrowRight: (that: Slider) => SliderMove(that, 'by', 1),
  ArrowLeft: (that: Slider) => SliderMove(that, 'by', -1),
  Digit1: (that: Slider) => SliderMove(that, 'to', 1),
  Digit2: (that: Slider) => SliderMove(that, 'to', 2),
  Digit3: (that: Slider) => SliderMove(that, 'to', 3),
  Digit4: (that: Slider) => SliderMove(that, 'to', 4),
  Digit5: (that: Slider) => SliderMove(that, 'to', 5),
  Digit6: (that: Slider) => SliderMove(that, 'to', 6),
  Digit7: (that: Slider) => SliderMove(that, 'to', 7),
  Digit8: (that: Slider) => SliderMove(that, 'to', 8),
  Digit9: (that: Slider) => SliderMove(that, 'to', 9),
};

/**
 * Listen for keyboard events
 * @param e Keydown Event
 * @example
 *   document.addEventListener('keydown', SliderKeysOnKeydown);
 *   document.removeEventListener('keydown', SliderKeysOnKeydown);
 */
const SliderKeysOnKeydown = (e: KeyboardEvent) => {
  const keys = Object.entries(SliderKeyboardControls) ?? null;

  if (currentSlider && keys) {
    for (let i = 0, n = Array.from(keys); i < n.length; i++) {
      /** Loop over {@link SliderKeyboardControls} and trigger functions if a matching {@link key} was pressed. */
      const item = n[i];
      const key = item[0];
      const fn = item[1];

      if (e.code === key) {
        fn(currentSlider);
        return;
      }
    }
  }
};

/**
 * Add keyboard event listeners
 * @param that Slider reference
 * @example
 *   SliderKeysAddEventListener(slider)
 */
export const SliderKeysAddEventListener = (that: Slider) => {
  if (!that.theSlider) return;

  // Listen for keydown event on document
  document.addEventListener('keydown', SliderKeysOnKeydown);

  // Set slider reference
  currentSlider = that;
};

/**
 * Remove keyboard event listeners
 * @example
 *   SliderKeysRemoveEventListener()
 */
export const SliderKeysRemoveEventListener = () => {
  // Stop listening for keydown event on document
  document.removeEventListener('keydown', SliderKeysOnKeydown);

  // Unset slider reference
  currentSlider = undefined;
};

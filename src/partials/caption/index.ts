import { Slider } from '../..';
import { findCaption } from '../helper';

/**
 * Build a caption element
 * @param that Slider reference
 * @param captionId Id of caption element that enables the use of the caption element as actual label for another element
 * @returns An element that serves as a caption for the slider
 * @example
 *   const captionElement = SliderCaptionBuild(slider, 'xxx-yyy')
 */
export const SliderCaptionBuild = (
  that: Slider,
  captionId: string,
): HTMLSpanElement | undefined => {
  if (!that.parameters) return undefined;
  const el = document.createElement('span');
  el.setAttribute('data-a11y-slider-caption', '');
  el.classList.add('visuallyhidden');
  el.id = captionId;
  el.innerText =
    that.parameters.caption || findCaption(that.parameters.slides) || 'Slider';
  return el;
};

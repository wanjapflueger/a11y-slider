import { uuid } from '../helper';
import { Button } from '@wanjapflueger/a11y-button';
import { Slider } from '../..';
import { SliderSlidesIsFirstSlide, SliderSlidesIsLastSlide } from '../slides';
import { SliderMove } from '../slider';

/**
 * Get previous arrow
 * @param that Slider reference
 * @returns Previous arrow
 * @example
 *   const prevArrow = SliderArrowsGetPrev(slider)
 */
const SliderArrowsGetPrev = (that: Slider) =>
  that.theSlider?.querySelectorAll('[data-a11y-slider-arrows] button')[0];

/**
 * Get next arrow
 * @param that Slider reference
 * @returns Next arrow
 * @example
 *   const nextArrow = SliderArrowsGetPrev(slider)
 */
const SliderArrowsGetNext = (that: Slider) =>
  that.theSlider?.querySelectorAll('[data-a11y-slider-arrows] button')[1];

/**
 * Build arrows
 * @param that Slider reference
 * @returns An element containing a previous and a next arrow
 * @example
 *   const arrows = SliderArrowsBuild(slider)
 */
export const SliderArrowsBuild = (that: Slider): HTMLDivElement | undefined => {
  if (!that.parameters || !that.id) return undefined;

  const el = document.createElement('div');
  el.setAttribute('data-a11y-slider-arrows', '');

  const id = uuid();
  const nav = document.createElement('nav');
  nav.setAttribute('aria-labelledby', id);

  const caption = document.createElement('span');
  caption.classList.add('visuallyhidden');
  caption.innerText =
    typeof that.parameters.arrows !== 'boolean'
      ? that.parameters.arrows?.label || 'Slider controls'
      : 'Slider controls';
  caption.id = id;

  const ul = document.createElement('ul');
  const liPrev = document.createElement('li');
  const liNext = document.createElement('li');

  const SliderArrowsPrev = document.createElement('button');
  if (typeof that.parameters.arrows === 'boolean') {
    SliderArrowsPrev.innerHTML = '&larr;';
  } else {
    if (typeof that.parameters.arrows?.prev?.icon === 'object') {
      SliderArrowsPrev.append(that.parameters.arrows.prev?.icon);
    } else {
      SliderArrowsPrev.innerHTML =
        that.parameters.arrows?.prev?.icon ||
        that.parameters.arrows?.prev?.label ||
        '&larr;';
    }
  }

  SliderArrowsPrev.setAttribute('aria-controls', that.id);
  SliderArrowsPrev.setAttribute('aria-disabled', 'true');
  SliderArrowsPrev.setAttribute('data-a11y-slider-arrow', '');
  SliderArrowsPrev.setAttribute('data-a11y-slider-previous', '');
  Button({
    el: SliderArrowsPrev,
    title:
      typeof that.parameters.arrows !== 'boolean'
        ? that.parameters.arrows?.prev?.label || 'Previous slide'
        : 'Previous slide',
    lang: that.parameters.lang,
  });

  const SliderArrowsNext = document.createElement('button');
  if (typeof that.parameters.arrows === 'boolean') {
    SliderArrowsNext.innerHTML = '&rarr;';
  } else {
    if (typeof that.parameters.arrows?.next?.icon === 'object') {
      SliderArrowsNext.append(that.parameters.arrows.next?.icon);
    } else {
      SliderArrowsNext.innerHTML =
        that.parameters.arrows?.next?.icon ||
        that.parameters.arrows?.next?.label ||
        '&rarr;';
    }
  }
  SliderArrowsNext.setAttribute('aria-controls', that.id);
  SliderArrowsNext.setAttribute('data-a11y-slider-arrow', '');
  SliderArrowsNext.setAttribute('data-a11y-slider-next', '');
  Button({
    el: SliderArrowsNext,
    title:
      typeof that.parameters.arrows !== 'boolean'
        ? that.parameters.arrows?.next?.label || 'Next slide'
        : 'Next slide',
    lang: that.parameters.lang,
  });

  /** Timeout for wheel event */
  let timeout: NodeJS.Timeout;

  /** Collect the pointerEvent values of each arrow */

  const SliderArrowsPrevPointerEvents = SliderArrowsPrev.style.pointerEvents;
  const SliderArrowsNextPointerEvents = SliderArrowsNext.style.pointerEvents;

  /**
   * On wheel callback for previous button. This function ensures, that it is possible to scroll the slider whilst hovering any of the sliders arrow buttons.
   * @example
   *   SliderArrowsPrev.addEventListener('wheel', onWheelPrev);
   */
  const onWheelPrev = () => {
    if (timeout) {
      clearTimeout(timeout);
    }

    SliderArrowsPrev.style.pointerEvents = 'none';

    timeout = setTimeout(() => {
      SliderArrowsPrev.style.pointerEvents = SliderArrowsPrevPointerEvents;
    }, 250);
  };

  /**
   * On wheel callback for next button. This function ensures, that it is possible to scroll the slider whilst hovering any of the sliders arrow buttons.
   * @example
   *   SliderArrowsNext.addEventListener('wheel', onWheelNext);
   */
  const onWheelNext = () => {
    if (timeout) {
      clearTimeout(timeout);
    }

    SliderArrowsNext.style.pointerEvents = 'none';

    timeout = setTimeout(() => {
      SliderArrowsNext.style.pointerEvents = SliderArrowsNextPointerEvents;
    }, 250);
  };

  /** Bind Event Listeners to {@link SliderArrowsPrev} */
  SliderArrowsPrev.addEventListener('wheel', onWheelPrev);
  SliderArrowsPrev.addEventListener('click', () => {
    SliderMove(that, 'by', (that.parameters?.slideBy || 1) * -1);
  });

  /** Bind Event Listeners to {@link SliderArrowsNext} */
  SliderArrowsNext.addEventListener('wheel', onWheelNext);
  SliderArrowsNext.addEventListener('click', () => {
    SliderMove(that, 'by', that.parameters?.slideBy || 1);
  });

  liPrev.append(SliderArrowsPrev);
  liNext.append(SliderArrowsNext);

  ul.append(liPrev);
  ul.append(liNext);
  nav.append(caption);
  nav.append(ul);
  el.append(nav);

  return el;
};

/**
 * Build arrows wrapper
 * @returns An element that is a wrapper for the slider arrows
 * @example
 *   const arrowsWrapper = SliderArrowsBuildWrapper()
 */
export const SliderArrowsBuildWrapper = (): HTMLDivElement => {
  const el = document.createElement('div');
  el.setAttribute('data-a11y-slider-arrows-wrapper', '');
  return el;
};

/**
 * Show or hide arrows depending on the sliders current position. Can be called at any time and repeatedly.
 * @param that Slider reference
 * @returns True indicates that any arrows have been updated
 * @example
 *   SliderArrowsOnUpdate(slider)
 */
export const SliderArrowsOnUpdate = (that: Slider): boolean => {
  if (!that.theSlider) return false;

  const prev = SliderArrowsGetPrev(that);
  const next = SliderArrowsGetNext(that);

  if (!prev || !next) return false;

  const first = SliderSlidesIsFirstSlide(that);
  const last = SliderSlidesIsLastSlide(that);

  if (that.elements.arrows) {
    if (!that.parameters?.loop === true) {
      if (that.elements.items.length > 1) {
        if (first && last) {
          that.elements.arrows.setAttribute('aria-disabled', 'true');
          that.elements.arrows.setAttribute('aria-hidden', 'true');
        } else {
          if (
            that.elements.arrows.getAttribute('aria-disabled') &&
            that.elements.arrows.getAttribute('aria-disabled') === 'true'
          ) {
            that.elements.arrows.removeAttribute('aria-disabled');
            that.elements.arrows.removeAttribute('aria-hidden');
          }
        }
      } else {
        that.elements.arrows.setAttribute('aria-disabled', 'true');
        that.elements.arrows.setAttribute('aria-hidden', 'true');
      }
    }
  }

  if (!that.parameters?.loop === true) {
    first
      ? prev.setAttribute('aria-disabled', 'true')
      : prev.removeAttribute('aria-disabled');

    last
      ? next.setAttribute('aria-disabled', 'true')
      : next.removeAttribute('aria-disabled');
  } else {
    next.removeAttribute('aria-disabled');
    prev.removeAttribute('aria-disabled');
  }

  return true;
};

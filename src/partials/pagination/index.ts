import { Button } from '@wanjapflueger/a11y-button';
import { Slider } from '../..';
import { uuid, isElementInViewport, isVisible } from '../helper';
import { SliderGetCurrentIndex, SliderMove } from '../slider';
import { SliderSlidesIsFirstSlide, SliderSlidesIsLastSlide } from '../slides';

/**
 * Build a pagination
 * @param that Slider reference
 * @returns An element that includes n bullets that control the slider's position
 * @example
 *   const pagination = SliderPaginationBuild(slider)
 */
export const SliderPaginationBuild = (
  that: Slider,
): HTMLDivElement | undefined => {
  if (!that.parameters || !that.id) return undefined;

  const el = document.createElement('div');
  el.setAttribute('data-a11y-slider-pagination', '');

  const id = uuid();
  const nav = document.createElement('nav');
  nav.setAttribute('aria-labelledby', id);

  const caption = document.createElement('span');
  caption.classList.add('visuallyhidden');
  caption.innerText =
    typeof that.parameters.pagination !== 'boolean'
      ? that.parameters.pagination?.label || 'Slider pagination'
      : 'Slider pagination';
  caption.id = id;

  const ul = document.createElement('ul');

  for (let i = 0, n = that.parameters.slides.children; i < n.length; i++) {
    const li = document.createElement('li');
    const bullet = document.createElement('button');
    bullet.setAttribute('aria-controls', that.id);
    bullet.setAttribute('data-a11y-slider-bullet', '');
    if (
      typeof that.parameters.pagination !== 'boolean' &&
      typeof that.parameters.pagination?.icon === 'object'
    ) {
      if (i === 0) {
        bullet.append(that.parameters.pagination.icon);
      } else {
        bullet.append(that.parameters.pagination.icon.cloneNode(true));
      }
    } else {
      if (typeof that.parameters.pagination === 'boolean') {
        bullet.innerText = (i + 1).toString();
      } else {
        if (that.parameters.pagination?.icon) {
          if (typeof that.parameters.pagination.icon === 'string') {
            bullet.innerText =
              that.parameters.pagination?.icon || (i + 1).toString();
          } else {
            bullet.append(that.parameters.pagination?.icon);
          }
        }
      }
    }
    Button({
      el: bullet,
      title: (i + 1).toString(),
      lang: that.parameters.lang,
    });
    bullet.addEventListener('click', () => {
      SliderMove(that, 'to', i + 1);
    });
    li.append(bullet);
    ul.append(li);
  }

  nav.append(caption);
  nav.append(ul);
  el.append(nav);

  return el;
};

/**
 * Get all pagination bullets
 * @param that Slider reference
 * @returns Pagination bullets
 * @example
 *   const bullets = SliderPaginationGetBullets(slider)
 */
const SliderPaginationGetBullets = (
  that: Slider,
): HTMLButtonElement[] | undefined => {
  return that.theSlider
    ? Array.from(
        that.theSlider.querySelectorAll('[data-a11y-slider-pagination] button'),
      )
    : undefined;
};

/**
 * Get all pagination bullets that are visible
 * @param that Slider reference
 * @returns Pagination bullets
 * @example
 *   const visibleBullets = SliderPaginationGetBulletsVisible(slider)
 */
const SliderPaginationGetBulletsVisible = (that: Slider) => {
  if (!that.theSlider) return undefined;

  return SliderPaginationGetBullets(that)?.filter(
    (bullet) => isVisible(bullet) && isElementInViewport(bullet, that),
  );
};

/** Last bullet */
let lastActive: HTMLButtonElement | undefined;

/**
 * Update the pagination depending on the current slider's position. Can be called anytime and repeatedly.
 * @param that Slider reference
 * @returns True if anything was changed
 * @example
 *   SliderPaginationOnUpdate(slider)
 */
export const SliderPaginationOnUpdate = (that: Slider) => {
  if (!that.theSlider) return false;

  /** All bullets */
  const bullets = SliderPaginationGetBullets(that);

  /** All visible bullets */
  const bulletsVisible = SliderPaginationGetBulletsVisible(that);

  const first = SliderSlidesIsFirstSlide(that);
  const last = SliderSlidesIsLastSlide(that);

  if (bullets && that.elements.pagination) {
    if (that.elements.items.length > 1) {
      if ((first && last) || bulletsVisible?.length === 1) {
        that.elements.pagination.setAttribute('aria-disabled', 'true');
        that.elements.pagination.setAttribute('aria-hidden', 'true');
      } else {
        if (
          that.elements.pagination.getAttribute('aria-disabled') &&
          that.elements.pagination.getAttribute('aria-disabled') === 'true'
        ) {
          that.elements.pagination.removeAttribute('aria-disabled');
          that.elements.pagination.removeAttribute('aria-hidden');
        }
      }
    } else {
      that.elements.pagination.setAttribute('aria-disabled', 'true');
      that.elements.pagination.setAttribute('aria-hidden', 'true');
    }
  }

  /** Current bullet */
  let current: HTMLButtonElement | undefined;

  // try to find a visible bullet, with the current slider index (may fail – which is okay)
  if (bulletsVisible && bulletsVisible.length) {
    if (last) {
      current = bulletsVisible[bulletsVisible.length - 1];
    } else {
      const i = SliderGetCurrentIndex(that) || 0;
      if (
        bullets &&
        isVisible(bullets[i]) &&
        isElementInViewport(bullets[i], that)
      ) {
        current = bullets[i];
      }
    }
  }

  // if still no current bullet was found, make the first bullet the current
  if (bullets && bullets.length && current === undefined) {
    current = bullets[0];
  }

  // only update if the new current bullet is not the old bullet
  if (current !== lastActive) {
    lastActive = current;

    // only change current bullet if the bullet would be visible
    if (current && bullets) {
      // Clear all
      for (let i = 0, n = Array.from(bullets); i < n.length; i++) {
        const item = n[i];
        if (item.parentElement?.hasAttribute('aria-current'))
          item.parentElement?.removeAttribute('aria-current');
      }

      // Mark currentBullet's list item as current
      if (current.parentElement?.getAttribute('aria-current') !== 'true')
        current.parentElement?.setAttribute('aria-current', 'true');
    }
  }

  return true;
};

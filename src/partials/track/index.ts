/**
 * Build track element
 * @returns An element serving as the slider track
 * @example
 *   const sliderTrack = SliderTrackBuild()
 */
export const SliderTrackBuild = (): HTMLDivElement => {
  const el = document.createElement('div');
  el.setAttribute('data-a11y-slider-track', '');
  return el;
};

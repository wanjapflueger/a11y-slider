import { polyfill as smoothscroll } from 'smoothscroll-polyfill';
import {
  SliderWatch,
  SliderModifyParameters,
  SliderSlidesBuildSlider,
  SliderMove,
} from './partials/slider';
import { uuid } from './partials/helper';

/** Prev- and next arrow */
interface Arrow {
  /** Visible text on arrow (default: {@link Arrow.label}) */
  icon?: HTMLElement | string;

  /** Title and label for screen reader (default: `'Previous slide'` or `'Next slide'`) */
  label: string;
}

/** Arrows including prev- and next arrow */
interface Arrows {
  /** Arrows label (default: `'Slider controls'`) */
  label?: string;

  /** Next arrow (required if {@link SliderParameters.arrows} is set) */
  next?: Arrow;

  /** Prev arrow (required if {@link SliderParameters.arrows} is set) */
  prev?: Arrow;
}

/** Pagination */
interface Pagination {
  /** Visible text on each bullet inside the pagination (default: index starting at `1` as `string`) */
  icon?: HTMLElement | string;

  /** Label for pagination (default: `'Slider pagination'`) */
  label?: string;
}

/**
 * The current status
 */
export interface Status {
  /** Progress (from `0` to `100`) represents how far the slider has been moved */
  progress: number;

  /** `true` if slider is showing the first slide */
  isFirst: boolean;

  /** `true` if slider is showing the last slide */
  isLast: boolean;

  /** Index value of the first visible slide */
  currentIndex: number;
}

/**
 * Slider Parameters
 */
export interface SliderParameters {
  /** Arrows navigation */
  arrows?: boolean | Arrows;

  /** Autoplay speed in milliseconds */
  autoplay?: number;

  /** A caption for the slider (default: `'Slider'`) */
  caption?: string;

  /** Language code like `'de'`, `'en'` (ISO 639-1, default: document language) */
  lang?: string;

  /** `true` enables you to go from the frist to last and from the last to first slide. */
  loop?: boolean;

  /** Pagination */
  pagination?: boolean | Pagination;

  /** Number of slides to scroll (default: `1`) */
  slideBy?: number;

  /** Any `HTMLElement` with children. The contents of each child will be used as the contents of a slide. */
  slides: HTMLElement;

  /** Slider movement will be synced with other instances of {@link Slider}. Any synced instances will be updated, when this sliders position has been changed (see {@link SliderParameters.onMoveEnd}). */
  syncWith?: Slider[];

  /** Callback function once the slider has been created successfully */
  onCreated?: () => void;

  /** Callback function once the slider has beed destroyed successfully */
  onDestroyed?: () => void;

  /** Callback function on update (resize, move, scroll, etc.) */
  onUpdate?: (status: Status) => void;

  /** Callback function once movement starts */
  onMoveStart?: (status: Status) => void;

  /** Callback function once movement ends */
  onMoveEnd?: (status: Status) => void;
}

/**
 * A11Y Slider. A fully accessible slider carousel.
 * @author Wanja Friedemann Pflüger <noreply@wanjapflueger.de>
 * @example
 *   const mySlider = new Slider()
 */
export class Slider {
  /** Parameters */
  parameters: SliderParameters | undefined;

  /** Unique identifier for this {@link Slider} */
  id: string | undefined;

  /** The slider element that will be inserted into the `DOM` */
  theSlider: HTMLDivElement | undefined;

  /**
   * Get the elements accosiated with this {@link Slider}
   * @returns Accosiated elements
   * @example
   *   mySlider.elements
   *   mySlider.elements.arrows
   *   ...
   */
  get elements() {
    /** Previous arrow */
    const prev = this.theSlider?.querySelector(
      '[data-a11y-slider-previous]',
    ) as HTMLButtonElement;

    /** Next arrow */
    const next = this.theSlider?.querySelector(
      '[data-a11y-slider-next]',
    ) as HTMLButtonElement;

    /** Wrapper for {@link prev} and {@link next} */
    const arrows = this.theSlider?.querySelector(
      '[data-a11y-slider-arrows]',
    ) as HTMLDivElement;

    /** The sliders caption */
    const caption = this.theSlider?.querySelector(
      '[data-a11y-slider-caption]',
    ) as HTMLSpanElement;

    /** Slides */
    const items = this.theSlider?.querySelectorAll(
      '[data-a11y-slider-slide]:not([data-a11y-slider-slide-spacer])',
    ) as NodeListOf<HTMLLIElement>;

    /** Container containing {@link items} */
    const list = this.theSlider?.querySelector(
      '[data-a11y-slider-slides]',
    ) as HTMLUListElement;

    /** Pagination containing {@link bullets} */
    const pagination = this.theSlider?.querySelector(
      '[data-a11y-slider-pagination]',
    ) as HTMLDivElement;

    /** Bullets */
    const bullets = this.theSlider?.querySelectorAll(
      '[data-a11y-slider-bullet]',
    ) as NodeListOf<HTMLButtonElement>;

    /** This element is a direct child of the slides container. It is used to create the space before the first slide. */
    const spacerBefore = this.theSlider?.querySelector(
      '[data-a11y-slider-slides] > li:first-child',
    ) as HTMLLIElement;

    /** This element is a direct child of the slides container. It is used to create the space after the last slide. */
    const spacerAfter = this.theSlider?.querySelector(
      '[data-a11y-slider-slides] > li:last-child',
    ) as HTMLLIElement;

    return {
      arrows,
      bullets,
      caption,
      items,
      list,
      next,
      pagination,
      prev,
      spacerAfter,
      spacerBefore,
    };
  }

  /**
   * Create slider
   * @param parameters Slider parameters
   * @returns `true` if successfull
   * @example
   *   mySlider.Create({
   *     slides: myList
   *   })
   */
  Create(parameters: SliderParameters): boolean {
    // Do not create a second instance from the reference element in case the slider has already been created.
    if (parameters.slides.hasAttribute('data-a11y-slider-reference'))
      return false;

    /** Create a unique ID for this {@link Slider.id} */
    this.id = uuid();

    /** Assign {@link parameters} to {@link Slider.parameters} */
    this.parameters = SliderModifyParameters(parameters);
    if (!this.parameters) return false;

    /** Build the HTML for the slider and assign the returned element to {@link Slider.theSlider} */
    this.theSlider = SliderSlidesBuildSlider(this);
    if (!this.theSlider) return false;

    /** Place {@link Slider.theSlider} slider in `DOM` */
    this.parameters.slides.parentElement?.insertBefore(
      this.theSlider,
      this.parameters.slides,
    );

    // Watch the slider and its elements, listen and react to changes
    SliderWatch(this);

    /** Hide {@link SliderParameters.slides} in `DOM` */
    this.parameters.slides.setAttribute('aria-hidden', 'true');
    this.parameters.slides.setAttribute('data-a11y-slider-reference', this.id);

    // Kick polyfill for smooth scrolling on ms egde
    smoothscroll();

    /** At this point, the slider was built and inserted into the `DOM` so we may now call any callback function in {@link SliderParameters.onCreated} to confirm that the slider has been created successfully. */
    if (this.parameters.onCreated) {
      this.parameters.onCreated.call(this);
    }

    return true;
  }

  /**
   * Destroy an existing slider
   * @returns `true` if successfull
   * @example
   *   mySlider.Destroy()
   */
  Destroy(): boolean {
    if (!this.theSlider || !this.parameters) {
      return false;
    }

    // Remove slider
    this.theSlider.remove();
    this.theSlider = undefined;

    // Show original list element
    this.parameters.slides.removeAttribute('aria-hidden');
    this.parameters.slides.removeAttribute('data-a11y-slider-reference');

    /** At this point the slider has been removed from the `DOM`. The original element that was originally assigned in {@link Slider.Create} ({@link SliderParameters.slides}) is visible again. We can now call any callback function in {@link SliderParameters.onDestroyed} to confirm that the slider has been destroyed successfully. */
    if (this.parameters.onDestroyed) {
      this.parameters.onDestroyed.call(this);
    }

    return true;
  }

  /**
   * Go to the next slide
   * @returns `true` if successfull
   * @example
   *   mySlider.Next();
   */
  Next(): boolean {
    if (!this.theSlider || !this.parameters) {
      return false;
    }

    SliderMove(this, 'by', 1);

    return true;
  }

  /**
   * Go to the previous slide
   * @returns `true` if successfull
   * @example
   *   mySlider.Prev();
   */
  Prev(): boolean {
    if (!this.theSlider || !this.parameters) {
      return false;
    }

    SliderMove(this, 'by', -1);

    return true;
  }

  /**
   * Go to a specific slide
   * @param slideNumber Slide count (`1` = first slide)
   * @returns `true` if successfull
   * @example
   *   mySlider.GoTo(3); // Go to slide 3
   */
  GoTo(slideNumber: number): boolean {
    if (!this.theSlider || !this.parameters) {
      return false;
    }

    SliderMove(this, 'to', slideNumber);

    return true;
  }
}

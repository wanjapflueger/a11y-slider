/**
 * @jest-environment jsdom
 */

// Jest Testing documentation @see {@link https://jestjs.io/docs/expect}

import { Slider } from '../index';

test('INIT    → is an instance of Slider', () => {
  const mySlider = new Slider();

  expect(mySlider instanceof Slider).toBe(true);
});

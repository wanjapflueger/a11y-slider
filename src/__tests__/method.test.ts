/**
 * @jest-environment jsdom
 */

// Jest Testing documentation @see {@link https://jestjs.io/docs/expect}

import { Slider } from '../index';

// Build a dummy list
const createList = () => {
  const items = ['A', 'B', 'C', 'D', 'E'];
  document.body.innerHTML = '';
  const list = document.createElement('ul');
  for (let i = 0; i < items.length; i++) {
    const li = document.createElement('li');
    li.innerHTML = items[i];
    list.append(li);
  }
  document.body.append(list);
  return list;
};

test('METHOD  → Create() creates a slider', () => {
  const mySlider = new Slider();
  const myList = createList();
  mySlider.Create({
    slides: myList,
  });

  expect(mySlider.theSlider).not.toBe(undefined);
  if (mySlider.theSlider)
    expect(mySlider.theSlider.nodeName === 'DIV').toBe(true);

  mySlider.Destroy();
});

test('METHOD  → Destroy() destroys the slider and restores the reference element', () => {
  const mySlider = new Slider();
  const myList = createList();
  mySlider.Create({
    slides: myList,
  });

  mySlider.Destroy();

  expect(mySlider.theSlider).toBe(undefined);
  expect(mySlider.elements.pagination).toBe(undefined);
  expect(mySlider.elements.arrows).toBe(undefined);
  expect(mySlider.elements.caption).toBe(undefined);
  expect(document.querySelectorAll('[data-a11y-slider]').length).toBe(0);
  expect(document.querySelectorAll('ul').length).toBe(1);
});

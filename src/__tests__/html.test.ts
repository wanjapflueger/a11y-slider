/**
 * @jest-environment jsdom
 */

// Jest Testing documentation @see {@link https://jestjs.io/docs/expect}

import { Slider } from '../index';

// Build a dummy list
const createList = () => {
  const items = ['A', 'B', 'C', 'D', 'E'];
  document.body.innerHTML = '';
  const list = document.createElement('ul');
  for (let i = 0; i < items.length; i++) {
    const li = document.createElement('li');
    li.innerHTML = items[i];
    list.append(li);
  }
  document.body.append(list);
  return list;
};

test('HTML    → Caption element exists and is a HTMLSpanElement', () => {
  const mySlider = new Slider();
  const myList = createList();

  mySlider.Create({
    caption: 'My caption',
    slides: myList,
  });

  const captionElements = document.querySelectorAll(
    '[data-a11y-slider] > [data-a11y-slider-caption]',
  );

  expect(captionElements.length).toBe(1);
  expect(captionElements[0].nodeName === 'SPAN').toBe(true);
  expect(
    mySlider.elements.caption &&
      mySlider.elements.caption.innerText === 'My caption',
  ).toBe(true);
  expect(
    mySlider.elements.caption &&
      mySlider.theSlider &&
      mySlider.elements.caption.id ===
        mySlider.elements.list.getAttribute('aria-labelledby'),
  ).toBe(true);

  mySlider.Destroy();
});

test('HTML    → Slides element exists and is an HTMLULElement', () => {
  const mySlider = new Slider();
  const myList = createList();

  mySlider.Create({
    slides: myList,
  });

  const list = document.querySelectorAll(
    '[data-a11y-slider] [data-a11y-slider-slides]',
  );

  expect(list.length).toBe(1);
  expect(list[0].nodeName === 'UL').toBe(true);

  mySlider.Destroy();
});

test('HTML    → Slider has no arrows by default', () => {
  const mySlider = new Slider();
  const myList = createList();

  mySlider.Create({
    slides: myList,
  });

  expect(
    document.querySelectorAll('[data-a11y-slider] [data-a11y-slider-arrows]')
      .length,
  ).toBe(0);

  mySlider.Destroy();
});

test('HTML    → Slider has no pagination by default', () => {
  const mySlider = new Slider();
  const myList = createList();

  mySlider.Create({
    slides: myList,
  });

  expect(
    document.querySelectorAll(
      '[data-a11y-slider] [data-a11y-slider-pagination]',
    ).length,
  ).toBe(0);

  mySlider.Destroy();
});

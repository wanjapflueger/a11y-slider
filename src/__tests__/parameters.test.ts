/**
 * @jest-environment jsdom
 */

// Jest Testing documentation @see {@link https://jestjs.io/docs/expect}

import { Slider } from '../index';

// Build a dummy list
const createList = () => {
  const items = ['A', 'B', 'C', 'D', 'E'];
  document.body.innerHTML = '';
  const list = document.createElement('ul');
  for (let i = 0; i < items.length; i++) {
    const li = document.createElement('li');
    li.innerHTML = items[i];
    list.append(li);
  }
  document.body.append(list);
  return list;
};

test('PARAM   → "arrows" as "boolean" with value "true"', () => {
  const mySlider = new Slider();
  const myList = createList();

  mySlider.Create({
    arrows: true,
    slides: myList,
  });

  expect(
    document.querySelectorAll('[data-a11y-slider] [data-a11y-slider-arrows]')
      .length,
  ).toBe(1);

  mySlider.Destroy();
});

test('PARAM   → "pagination" as "boolean" with value "true"', () => {
  const mySlider = new Slider();
  const myList = createList();

  mySlider.Create({
    pagination: true,
    slides: myList,
  });

  expect(
    document.querySelectorAll(
      '[data-a11y-slider] [data-a11y-slider-pagination]',
    ).length,
  ).toBe(1);

  mySlider.Destroy();
});

test('PARAM   → "caption" as "string"', () => {
  const mySlider = new Slider();
  const myList = createList();

  mySlider.Create({
    caption: 'My caption',
    slides: myList,
  });

  expect(
    mySlider.theSlider &&
      (
        mySlider.theSlider.querySelector(
          '[data-a11y-slider-caption]',
        ) as HTMLSpanElement
      ).innerText === 'My caption',
  ).toBe(true);

  mySlider.Destroy();
});

test('PARAM   → "lang" as "string"', () => {
  const mySlider = new Slider();
  const myList = createList();

  mySlider.Create({
    arrows: true,
    pagination: true,
    lang: 'fr',
    slides: myList,
  });

  if (mySlider.theSlider) {
    const buttons = document.querySelectorAll('button');

    buttons.forEach((button) => {
      expect(button.lang === 'fr').toBe(true);
    });
  }

  mySlider.Destroy();
});

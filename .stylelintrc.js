module.exports = {
  extends: ['stylelint-config-recommended', 'stylelint-prettier/recommended'],
  plugins: [
    'stylelint-scss',
    'stylelint-order',
    'stylelint-at-rule-no-children',
    'stylelint-prettier',
  ],
  rules: {
    'aditayvm/at-rule-no-children': [
      {
        ignore: [
          'content',
          'each',
          'else',
          'error',
          'extend',
          'for',
          'function',
          'if',
          'mixin',
          'return',
          'warn',
        ],
      },
    ],
    'at-rule-empty-line-before': [
      'always',
      {
        except: ['blockless-after-same-name-blockless', 'first-nested'],
        ignoreAtRules: ['else', 'import', 'include'],
      },
    ],
    'declaration-empty-line-before': [
      'always',
      {
        except: ['after-declaration', 'first-nested'],
      },
    ],
    'no-invalid-position-at-import-rule': [
      true,
      {
        ignoreAtRules: ['use'],
      },
    ],
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: [
          'content',
          'each',
          'else',
          'error',
          'use',
          'extend',
          'for',
          'function',
          'if',
          'include',
          'mixin',
          'return',
          'warn',
        ],
      },
    ],
    'order/order': ['declarations', 'custom-properties'],
    'order/properties-alphabetical-order': true,
    'rule-empty-line-before': [
      'always',
      {
        except: ['after-single-line-comment', 'first-nested'],
      },
    ],
    'scss/declaration-nested-properties': 'never',
  },
};
